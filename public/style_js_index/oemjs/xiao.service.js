var XiaoChengKF = {
    host: document.currentScript.src.split("/public")[0],
    boss_id: '',
    service_id: '',
    v_id:'',
    v_name:'',
    logo: '',
    title: '',
    hidebnt:false,//是否隐藏默认的 在线咨询悬浮按钮
    isopen: false,
    iscontent: false,
    contenttitle: "小二鸽客服平台欢迎您！<br>客服热线:1234567",
    openDelay: 1,
    theme: '#00BF6D',//主题色
    openmin: true,
    width: '400px',
    height: '620px',
    type: 'pcmin',
    url:''
};
(function () {
    var head = document.getElementsByTagName('script')[0];
    if (typeof (jQuery) == 'undefined') {
        var script = document.createElement('script');
        script.src = XiaoChengKF.host + '/public/style_js_com/jquery/jquery-3.3.1.min.js';
        head.parentNode.insertBefore(script, head);
    }
    if (typeof (layer) == 'undefined') {
        var link = document.createElement('script');
        link.src = XiaoChengKF.host + '/public/style_js_com/layer/layer.js';
        head.parentNode.insertBefore(link, head);
    }
    var link = document.createElement('link');
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.href = XiaoChengKF.host + '/public/style_js_com/layer/theme/default/layer.css?v=3.1.1';
    head.parentNode.insertBefore(link, head);
})()
//初始参数
XiaoChengKF.initConfig =function (initObjc={}) {
    if (typeof (initObjc) != 'undefined') {
        for (var inik in XiaoChengKF) {
            if (typeof (initObjc[inik]) != 'undefined') {
                XiaoChengKF[inik] = initObjc[inik];
            }
        }
    }
}

XiaoChengKF.init = function (initObjc = {}) {

    XiaoChengKF.initConfig(initObjc);
    XiaoChengKF.v_id=getCookie("visitor_id" + XiaoChengKF.boss_id);

    XiaoChengKF.addStyle();

    XiaoChengKF.initDbSetlink(XiaoChengKF.boss_id+"_"+XiaoChengKF.service_id,function (res) {
        XiaoChengKF.logo=res.data.logo;
        XiaoChengKF.title=res.data.title;
        var _theme = XiaoChengKF.theme;
        if(_theme.slice(0,1)=="#"){
            _theme= _theme.slice(1);
        }
        XiaoChengKF.url=XiaoChengKF.host +res.data.url+"?";
        var _pr="theme=" + _theme+(XiaoChengKF.v_id==""?"":"&v_id="+XiaoChengKF.v_id)+(XiaoChengKF.v_name==""?"":"&v_name="+XiaoChengKF.v_name);
        XiaoChengKF.url=XiaoChengKF.url+_pr;
        XiaoChengKF.title=XiaoChengKF.deftitle(XiaoChengKF.logo,XiaoChengKF.title);



        if (XiaoChengKF.isopen) {
            setTimeout(function () {
                XiaoChengKF.showKeFuBox();
            }, XiaoChengKF.openDelay * 1000)

        }
        setTimeout(function () {
            visiterAddLog();
        },  1000)

    })






}
XiaoChengKF.initDbSetlink=function (bs_id="",func =funciton()) {

        // 创建一个新的 XMLHttpRequest 对象
        var xhr = new XMLHttpRequest();
        // 设置请求的 URL，这里替换为您想要请求的 API URL
        var url = XiaoChengKF.host+'/setlink/config/'+(bs_id!="_"?bs_id:document.currentScript.src.split("?")[1]);
        // 设置请求类型为 GET，并指定响应类型为 JSON（如果服务器支持）
        xhr.open('GET', url, true, null, null, true, 'application/json');
        // 处理请求的完成状态，无论成功还是失败都会调用这个函数
        xhr.onload = function() {
            if (xhr.status === 200) {
                // 请求成功，处理返回的数据（这里假设服务器返回的是 JSON 格式）
                var data = JSON.parse(xhr.responseText); // 解析 JSON 数据
                func(data)
            } else {
                // 请求失败，处理错误情况（这里只是简单地在控制台打印错误信息）
                console.error('AJAX 请求失败:', xhr.statusText);
            }
        };
        // 处理可能的错误（如网络错误）
        xhr.onerror = function() {
            console.error('网络错误');
        };
        // 发送请求（由于我们设置了异步标志为 true，所以这里不会立即得到响应）
        xhr.send(); // 不需要传递任何参数，因为 GET 请求没有请求体（body）


}

XiaoChengKF.deftitle = function (logo, title) {
    if(logo!='' && logo.indexOf("://")==-1){
        logo = XiaoChengKF.host + logo;
        XiaoChengKF.logo=logo;
    }
    var logosrc = logo == '' ? '' : '<img id="XiaoChengKFfloatImg" src="' + logo + '" style="max-height:50px;max-width:140px;margin-left: 10px;vertical-align: middle;" />';
    return '<div style="line-height: 60px;height: 60px;vertical-align: middle;">' + logosrc + '<h1 style="position: absolute;left: 25%;width: 50%;height: 60px;margin: 0;line-height: 60px;font-size: 16px;text-align: center;display: inline-block;" id="XiaoChengKFfloatTitle">' + title + '</h1></div>';
}


XiaoChengKF.addStyle = function () {
    var _backgroud = (XiaoChengKF.theme + "").split("#");
    if (_backgroud.length > 1) _backgroud = "#" + _backgroud[1].slice(0, 6);
    //右下角浮动窗
    var styel ='' //'.goSkin .layui-layer-openwinother{background:url('+XiaoChengKF.host+'/public/style_js_index/image/icons/icondown.png) no-repeat 0;width:22px;}#layui-layer960088889{box-shadow:0 5px 40px rgb(0 0 0 / 16%);border-radius:12px;border:none;animation:up 0.5s ease-in-out both;}.goSkin .layui-layer-title{background-color:' + _backgroud + ';background-image:' + XiaoChengKF.theme + ';height:60px;border:0px;color:#fff;padding:0px;line-height:normal;border-radius:12px 12px 0 0;}.fixlButton{z-index:9999999999999!important;background:' + _backgroud + ';padding:8px;cursor:pointer!important;outline:0!important;display:flex;justify-content:center;align-items:center;margin:0!important;-webkit-font-smoothing:antialiased!important;-webkit-tap-highlight-color:transparent!important;color:#ffffff;position:relative;box-sizing:border-box!important;border-radius:28px;}';
    styel = styel+'.goSkin .layui-layer-openwinother{    position: absolute;;right: 12px;top: 17px;height: 22px;cursor: pointer;;background:url('+XiaoChengKF.host+'/public/style_js_index/image/icons/icondown.png) no-repeat 0;width:22px;}#layui-layer960088889{box-shadow:0 5px 40px rgb(0 0 0 / 16%);border-radius:12px;border:none;animation:up 0.5s ease-in-out both;}.goSkin .layui-layer-title{background-color:' + _backgroud + ';background-image:' + XiaoChengKF.theme + ';height:60px;border:0px;color:#fff;padding:0px;line-height:normal;border-radius:12px 12px 0 0;}.fixlButton{z-index:9999999999999!important;background:' + _backgroud + ';padding:8px;cursor:pointer!important;outline:0!important;display:flex;justify-content:center;align-items:center;margin:0!important;-webkit-font-smoothing:antialiased!important;-webkit-tap-highlight-color:transparent!important;color:#ffffff;position:relative;box-sizing:border-box!important;border-radius:28px;}';
    styel = styel+'.goSkin .layui-layer-openwin{    position: absolute;;right: 52px;top: 17px;height: 22px;cursor: pointer;;background:url('+XiaoChengKF.host+'/public/style_js_index/image/icons/iconmax.png) no-repeat 0;width:22px;}#layui-layer960088889{box-shadow:0 5px 40px rgb(0 0 0 / 16%);border-radius:12px;border:none;animation:up 0.5s ease-in-out both;}';
    styel=styel+'.layui-nav-childdd{display:none;color: rgba(0,0,0,.8);position:absolute;right:10px;margin:0;top:60px;min-width:100px;line-height:36px;padding:5px 0;box-shadow:0 2px 4px rgba(0,0,0,.12);border:1px solid #eee;background-color:#fff;z-index:100;border-radius:2px;white-space:nowrap;box-sizing:border-box;}';
    styel=styel+'.layui-nav .layui-nav-item a{display:block;padding:0 20px;transition:all .3s;-webkit-transition:all .3s;font-size: 14px;}dd{margin: 0;padding:0;text-align: center;font-size:14px;cursor: pointer;}';
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('style');
    link.type = 'text/css';
    link.id = 'goSkin-style';
    link.innerHTML = styel;
    head.appendChild(link);

    //下面是悬浮的在线客服
    if(XiaoChengKF.hidebnt){
        //如果配置不显示就关闭
        return;
    }
    var textkfstring ='<span style="font-size: 14px">在线咨询</span>';
    if( /Mobile|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        textkfstring='';
    }
    var floatbont = '<div onclick="XiaoChengKF.showKeFuBox();" id="XiaoChengKFfloatbont"  style="z-index: 999999;left: auto;position: fixed!important;bottom: 25px;right: 30px;">\n' +
        '<div id="XiaoChengKFfloatMsgnum" style="position: absolute;top:-16px;left: -14px;background: red;display: none;border-radius: 50%;width: 24px;height: 24px;line-height: 24px;text-align: center;color: white"></div>'+
        '    <div id="fixlButton" class="fixlButton">\n' +
        '        <div class="fixlButtonText">\n' +
        '            <img style="vertical-align: middle;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAA5ZJREFUWEe1l2uo5lMUxn/PB3L/IErJZYQYFB/QcUskYVzC1EyUJswkhMhMMwoJzSSKmjRGHRM5brnLB5cJYyYyJbnfIqIYUkJSj57T3sd//vM/73nf13t2vR/O2Ws969lrr/9azxZ9LtvbAycBpwLnAYe0XD8BngFeBd6Q9Hc/0JrJyPaJwCXAWcBeM9mX/R+B54A1kt7r5TMtAdsnA9eU01aMf4C3gFeADS3gY4BTgDFgt8beGuA2Sd91EekkYPsKYHXDIacI0POSfuh1Its7AqcBNwDJXtZnwE2Snmj7bkPA9rXAPcXwV2CZpAQfeNm+Dri74bhC0h1NoK0I2J4PPF4Mcup5knKfQy/b+wPrgf0KyGJJD1TAKQK2zwGeLRs/S9pz6KgdjrZ/AvYoW+dKSpHSJJDiOr4YHCbpoxETmAt8WDA3SDphioDtG4GVZXObexoVEdvLgdsL3lJJqyYzYPtL4ADgQUmXjSpgF47ttcClwMeS5sr2mcCLxfggSV/MMoEDgc9LjEkC9wNLgLcl1RqYTQ7J+OtAGt3iEHgHOBq4VdItsxq5gDdqbl0IfA3kW10ybMMZlHRp88nC+hD4HdgZOFvSC00w2+npK4A/J42lNJTJVUAuAlLAm6bZy2e3TtIvLdykf4rAFmB34EJJT7UMTwdeLv9bK+nyBoFlwJ197F0s6ZFeBDYBxwLLJVXAesozgJf6CNIml1q6ufjNSOBhIKkcl7SofZ+2A/ZXjzR3XUFqKgTeBSY6riADKoNqPDVQ2W6UdNygBTWMfaPxLQqB2qO3SKrDYhjcvnxsHw58UIzn1FYcdZPTr5K0tC+kIY0ajS9fzlglcD1wV8FcIOmxIfF7utm+Gri3GC2UNFEJ7FDmQTTdN5FUkmq/HgkX2/nS3gS2K4W5MMBNPXBoITEHeFrS+SOJ/F/j2gwcBUS+X1D1RluSNSfjTpLSAf/Xsj0PSF1NChBgK7HTJUq/BfYBjpT0/rDRbSeTCZxJW9eYpDS+qdVFoEqz+ZKeHJSA7cz7dNAE37v4/5b3RXNeVNwuAo8CC2prtr1r6flXAn8UjR+dn9+nQMDzKMn9HgHs2yKd59rK9sl7EYg2jEYcL0Gi43YZMBMZ8a8BD0lK5U+7ujJwFXAf8H0jhQFIn0jDinbM/dZf/k42cnUbgc2DKOouAs33QQJPRBNI+mrALPRl3kXgYCATMi+i1ZKqHugLcFCjfwE1LH9c1F9nvgAAAABJRU5ErkJggg==" />\n' +
        textkfstring +
        '        </div>\n' +
        '    </div>\n' +
        '\n' +
        '</div>';

    var floatcontent='<div id="XiaoChengKFfloatcontent" style="position:fixed;top:50%;left:50%;transform:translate(-50%,-50%);width:358px;background:#fff;border-radius:13px;z-index:555"><div class=polit-popup-cont><div style="width:86.4px;height:86.4px;background: url(\''+XiaoChengKF.logo+'\') no-repeat;background-size:100% 100%;position:absolute;left:141px;margin-top:-40px;z-index:999"></div><div style=height:160px;overflow:hidden;text-align:center><div style=margin-top:46.72px;font-size:16.64px;color:#555;letter-spacing:1.28px;line-height:28.8px>' +
        ''+XiaoChengKF.contenttitle+'</span></div><div style="text-align:center;cursor:pointer;overflow:hidden;padding:10.24px 57.6px 12.8px"><a  href=javascript:; onclick="XiaoChengKF.showKeFuBox()" style= "background-color:'+_backgroud+';color:#fff;font-weight:700;display:inline-block;width:115.2px;height:32px;font-size:14px;line-height:32px;border-radius:16px;text-decoration: none;">在线咨询</a></div></div></div><img onclick="$(`#XiaoChengKFfloatcontent`).remove();" class=polit-popup-close style="width: 36px;position: absolute;top: -4px;right: -6px;" data-wmdot=tap data-wmdot-id=window_close data-wmdot-type=popup src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAABmklEQVRoge2YQWrCUBCG/4l7oWfoouJSoifoFURBj6fY4hU8gXl0GXThGQrufdNFGkIlhiaZPJD838pI/Jkvb9RhAEIIIYQQQgghhJA6OJdOnDvN7fJOc+fSiVUeAERWQc6lE1UcvNddkqTLtnlJki69150qDpbSYhGSy6ri5Tf2JqLr6XT80SQvSdKlqmwAHQCACL5F8B7H46+2tRqdcPSqKsPiWgeqsjkez4u6SfeyAJBlR68mlVqExPFoL6JrQG7FuzoA/LaO9PF4XtzL5t0Sx6O9Ra0mLZ1TdjrZQ4hWs9nbZ9Vnswfjt2WyTb8aZZgKA82kQ8kCHQgDjwXKpOvca0EnwsD/RELLAh0KA9VC2euwskDHwkCVNBBaFgggDDyS/lNGEFkgkDBQJR1OFjCcpZ8FtrQ1vfrR6tXfUq8Gj16Nlk0EQkqbCrcpPJS0mbBFwSGkTQaPbFPZvtDs3mhVtjmx2oYaTVr+IqLX4rr5qZRJZ9n+YlGpWUsXm0sZWrRg3t4ierXaWJrzDIt4QgghhBBCCCGE9IAf7ohrQl2WJ1IAAAAASUVORK5CYII=" /></div>';

    if(XiaoChengKF.iscontent){
        floatbont+=floatcontent;
    }

//新建一个div元素节点   悬浮的在线客服
    var div=document.createElement("div");
    div.innerHTML = floatbont;
//把div元素节点添加到body元素节点中成为其子节点，但是放在body的现有子节点的最后
    document.body.appendChild(div);
//插入到最前面
    document.body.insertBefore(div, document.body.firstElementChild);


}

function XiaoChengKFfloatMsgnum() {
    //消息条数消失重置

    var boxshowmsgnum = document.getElementById('XiaoChengKFfloatMsgnum');
    if (boxshowmsgnum) {
        boxshowmsgnum.style.display = "none";
        boxshowmsgnum.innerText=0;
    }
}

XiaoChengKF.showKeFuBox = function () {
    //消息条数消失重置
    XiaoChengKFfloatMsgnum();

    var chatUrl = XiaoChengKF.url;
    if (!XiaoChengKF.openmin) {
        window.open(chatUrl);
        return;
    }
    var boxshow = document.getElementById('layui-layer960088889');
    if (boxshow) {
        boxshow.style.display = "block";
        return
    }
    layer.index = "960088888";
    var offset = [window.innerHeight - parseInt(XiaoChengKF.height) - 25, window.innerWidth - parseInt(XiaoChengKF.width) - 20];
    var area = [XiaoChengKF.width, XiaoChengKF.height];

    if( /Mobile|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
         offset = [100, 0];
        area = [window.innerWidth+"px", (window.innerHeight-100)+"px"];

    }


    layer.open({
        type: 2,
        title: XiaoChengKF.title,
        skin: "goSkin",
        closeBtn:0, //不显示关闭按钮
        shade: 0,
        area: area,
        offset: offset, //右下角弹出
        anim: 2,
        content: [chatUrl+"&type="+XiaoChengKF.type, 'yes'], //iframe的url，no代表不显示滚动条
        move: true,
        success: function (layero, index) {
            jQuery(".layui-layer-resize").remove();
            jQuery(".layui-layer-setwin").remove();
            jQuery("#layui-layer960088889").append(`<dl id="showwinother"  class="layui-nav-childdd" style="display: none;"><dd onclick="lookEwm()"><a>📲手机上查看</a></dd><dd onclick="editVisiterEmail()"><a>📧接收的email</a></dd><hr><dd onclick="window.location=&quot;about:blank&quot;" ><a>❌结束对话</a></dd><dd onclick="document.getElementById('layui-layer960088889').style.display='none'" ><a>➖小化对话</a></dd></dl>`);
            jQuery("#layui-layer960088889").append("<span class=\"layui-layer-openwinother \"></span>");
            jQuery("#layui-layer960088889").append("<span class=\"layui-layer-openwin \"></span>");
            jQuery(".layui-layer-openwin").on("click", function(){
                window.open(XiaoChengKF.url,XiaoChengKF.title,"top=20,left=20,location=no")
            });
            jQuery("#showwinother").on("click", function(){
                jQuery("#showwinother").hide();
            });
            jQuery(".layui-layer-openwinother").on("click", function(){
                if (document.getElementById("showwinother").style.display=="none"){
                    jQuery("#showwinother").show();
                }else{
                    jQuery("#showwinother").hide();
                }

            });


            },
        end: function () {
            jQuery(".launchButtonBox").show();

        },
        cancel: function (index, layero) {
            jQuery("#layui-layer960088889").hide();
            jQuery(".launchButtonBox").show();
            XiaoChengKFfloatMsgnum();
            return false;
        }
    });
}

function lookEwm () {
// 获取iframe元素
    var iframe = document.getElementById("layui-layer-iframe960088889");

    // 向iframe发送消息
    iframe.contentWindow.postMessage("lookEwm", "*");
}
function editVisiterEmail () {
// 获取iframe元素
    var iframe = document.getElementById("layui-layer-iframe960088889");

    // 向iframe发送消息
    iframe.contentWindow.postMessage("editVisiterEmail", "*");
}

//父页面接收：
window.addEventListener('message', function (event) {
    if(event.data.title && event.data.title!=""){
        document.getElementById('XiaoChengKFfloatTitle').innerText=event.data.title;
    }
    if(event.data.msgnum && event.data.msgnum!=""){
        document.getElementById('XiaoChengKFfloatMsgnum').innerText=event.data.msgnum;
        if(event.data.msgnum>0){
            document.getElementById('XiaoChengKFfloatMsgnum').style.display="block";
        }else{
            document.getElementById('XiaoChengKFfloatMsgnum').style.display="none";
        }

    }
})


var RefererKF=window.location.href;
setCookie('Referer', RefererKF);
setCookie('visitoravatar', ''); //访客头像
function setCookie(objName, objValue, objHours){//添加cookie
    var str = objName + "=" + encodeURIComponent(objValue);
    if (objHours > 0) {//为0时不设定过期时间，浏览器关闭时cookie自动消失
        var date = new Date();
        var ms = objHours * 3600 * 1000;
        date.setTime(date.getTime() + ms);
        str += "; expires=" + date.toUTCString();
    }
    document.cookie = str;

}
//获取 cookie
function getCookie(objName){//获取指定名称的cookie的值
    // 拆分 cookie 字符串
    var cookieArr = document.cookie.split(";");
    // 循环遍历数组元素
    for(var i = 0; i < cookieArr.length; i++) {
        var cookiePair = cookieArr[i].split("=");
        /* 删除 cookie 名称开头的空白并将其与给定字符串进行比较 */
        if(objName == cookiePair[0].trim()) {
            // 解码cookie值并返回
            return decodeURIComponent(cookiePair[1]);
        }
    }
    // 如果未找到，则返回null
    return "";
}
function getWebIcon() {
  var  link = document.querySelector(`link[rel='icon']`) ||
        document.querySelector(`link[rel='shortcut icon']`);
  if(link!=null){

  }
}

function visiterAddLog(title="") {

    if(window.location.href.indexOf("/service/setlink?")!=-1){
        return;
    }
    if (XiaoChengKF.v_id==""){
        XiaoChengKF.v_id=XiaoChengKF.boss_id+"z-"+new Date().getTime()+"-"+Math.round(Math.random()*80+20);
        setCookie("visitor_id" + XiaoChengKF.boss_id,XiaoChengKF.v_id);
    }
    if (title==""){
        title="访客访问："+document.title;
    }
    $.ajax({
        url: XiaoChengKF.host+'/api/visitor/addlog',
        type: "post",
        data: {
            visiter_id:XiaoChengKF.v_id,
            boss_id:XiaoChengKF.boss_id,
            title:title
        },
        success: function (res) {

        },
        error: function (data) {

        }
    });
}

