
var timds=(Date.parse(new Date())+"").slice(0,10)-0;
var $,table,form,table_elem_id;
layui.use(['table','form'], function(){
     $ = layui.$
     ,table = layui.table
     ,form = layui.form
     ,table_elem_id='layui-data-table';



    table.render({
      elem: '#'+table_elem_id
       ,method:'post'
      ,url: '?'
      ,parseData: function(res){ //res 即为原始返回的数据
            res =DianJinAjaxSuccess(res);
        return {
          "code": res.code==200?0:res.code, //解析接口状态
          "msg": res.msg, //解析提示文本
          "count": res.data.page.Total, //解析数据长度
          "data": res.data.data //解析数据列表
        };
      }
       ,defaultToolbar: ['filter','exports','print']
      ,toolbar:'#layui-data-table-toolbar'
      ,height: 'full-100'
      ,cellMinWidth: 80
      
      ,page: true
      ,limit: 15
      ,cols: [[
        {title: '序号',templet: '#indexTpl',type:'numbers', fixed: 'left' }
       // ,{type:'checkbox', fixed: 'left' }
        ,{field:'ServiceId',title: '客服ID', width: 80}
            ,{field:'Avatar',title: '头像', width: 60,templet: function(res){
                    return '<img src="'+res.Avatar+'" width="25" height="25">';
                }}
        ,{field:'UserName',title: '用户名'}

        ,viptf('reg')?{
                field: 'Otime', title: '到期时间',width: 260, templet: function (res) {
                    if(res.UserName=='root'){
                        return '';
                    }
                    var rstrin=""
                    if(res.Otime==0){
                        rstrin= ""
                    }else{
                        rstrin= formatDateTime(res.Otime)
                    }
                    var colorsk=""
                    if(timds-res.Otime>0){
                        colorsk="color:red"
                    }
                    var typec=["免费版","标准版","专业版"]
                    return typec[res.Otype]+' <span  style="'+colorsk+'" >'+rstrin+'</span>'+' <a class="layui-btn layui-btn-primary layui-btn-xs " style="'+colorsk+'" lay-event="addOtime">加时间</a>'
                }
            }:{width: 0},{field:'NickName',title: '昵称'}


        , {
                field: 'State', title: '状态', templet: function (res) {
                    if (res.State == 1) {
                        return '<span style="color:green">正常</span>';
                    } else if (res.State == 9) {
                        return '<span style="color:#FF5722">停用</span>';
                    } else {
                        return '<span style="color:darkgrey">正常</span>';
                    }

                }
            }
            ,{field:'ServiceType',title: '账号类型',width: 180,templet: function(res){
                    var sexx=['','<span style="color:darkgrey">超级管理员</span>','<span style="color:green">主客服</span>','<span style="color:green">客服小二@主号id:'+res.Groupid+'</span>'];
                    return sexx[res.ServiceType];
                }}
            ,  {
                field: 'State', title: '操作', width: 200, templet: function (res) {
                    if(res.UserName=='root'){
                        return '';
                    }


                    return '\n' +res.ServiceId+
                        (res.State==9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs layui-border-green" lay-event="edit"><i class="layui-icon layui-icon-edit"></i></a>\n') +
                        (res.State==9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs layui-border-red" lay-event="del">停用</a>\n') +
                        // (res.State==9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs " lay-event="webadd"><i class="layui-icon"> </i>产品接入</a>\n') +

                        (res.State!=9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs layui-border-red" lay-event="deleuser">删除</a>\n') +
                        (res.State!=9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs " lay-event="del">启用</a>\n') +
                        (res.ServiceType==1 ||res.ServiceType==3 ?'': '<a class="layui-btn layui-btn-primary layui-btn-xs " lay-event="addkfx2">新增小二</a>\n') +
                        '                            <!--<a class="layui-btn layui-btn-primary layui-btn-xs layui-border-blue" lay-event="info"><i class="layui-icon layui-icon-right"></i></a>-->\n' +
                        '                       '


                }
            }
       /* ,{fixed: 'right', width: 190,title:'操作',templet: function(res){


            if(res.UserName=='root'){
                return '';
            }


            return ' \n' +
                '\n' +
                (res.State==9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs layui-border-green" lay-event="edit"><i class="layui-icon layui-icon-edit"></i></a>\n') +
                (res.State==9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs layui-border-red" lay-event="del">停用</a>\n') +
               // (res.State==9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs " lay-event="webadd"><i class="layui-icon"> </i>产品接入</a>\n') +

                (res.State!=9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs layui-border-red" lay-event="deleuser">删除</a>\n') +
                (res.State!=9?'': '                            <a class="layui-btn layui-btn-primary layui-btn-xs " lay-event="del">启用</a>\n') +
                (res.ServiceType==1 ||res.ServiceType==3 ?'': '<a class="layui-btn layui-btn-primary layui-btn-xs " lay-event="addkfx2">新增小二</a>\n') +
                '                            <!--<a class="layui-btn layui-btn-primary layui-btn-xs layui-border-blue" lay-event="info"><i class="layui-icon layui-icon-right"></i></a>-->\n' +
                '                       '

                }
      }*/

      ]]
    });


    var submitFuc=function(index, layero){
        var iframeWindow = window['layui-layer-iframe'+ index]
            ,submitID = 'LAY-user-role-submit'
            ,submit = layero.find('iframe').contents().find('#'+ submitID)
            ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
            if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;
        //监听提交
        iframeWindow.layui.form.on('submit('+submitID+')', function(data){
            var field = data.field; //获取提交的字段
            var fieldint = (inputtypeints+"").split(",");
            var BossIds = [];
            for (d in field){
                if (fieldint.indexOf(d)!=-1){
                    field[d]=field[d]-0;
                }
                if((d+"").indexOf("BossIds[")!=-1){
                    BossIds.push(field[d])
                }
            }

            field.BossIds=BossIds.join(",")

            console.log(fieldint)
            console.log(field)
            //提交 Ajax 成功后，静态更新表格中的数据
            DianJinAjax("1",{dataCode: true,resCode: false},{
                url:'./save',
                type:'POST',
                data:field,
                success:function(result){
                    layer.msg(result.msg)
                    if(result.code==200){
                        table.reload(table_elem_id); //数据刷新
                        layer.close(index); //关闭弹层
                    }

                }});

        });

        submit.trigger('click');
    }

    var add=function(type="",id=""){
     layer.open({
                type: 2
                ,title: '新增'
                ,content: './add?type='+type+"&kf="+id
                ,area: layer_open_area(['420px', '620px'])
                ,maxmin: true
                ,btn: ['确定', '取消']
                ,yes: submitFuc
            });
    };
     var addOtimeFuc=function(Obj){
         var url='./addotime?otime='+Obj.Otime+"&s_id="+Obj.ServiceId+"&otype="+Obj.Otype
        layer.open({
            type: 2
            ,title: Obj.UserName+":"+Obj.NickName+" 修改到期时间|版本"
            ,content:url
            ,area: layer_open_area(['420px', '620px'])
            ,maxmin: true
            ,btn: ['新窗口打开', '取消']
            ,yes: function () {
                window.open(url)
            }
            ,cancel:function (index) {
                table.reload(table_elem_id); //数据刷新
                layer.close(index); //关闭弹层
            },btn2:function (index) {
                table.reload(table_elem_id); //数据刷新
                layer.close(index); //关闭弹层
            }
        });
    };
    var edit=function(id){
     layer.open({
                type: 2
                ,title: '修改'
                ,content: './edit?id='+id
                ,maxmin: true
                ,area: layer_open_area(['420px', '620px'])
                ,btn: ['确定', '取消']
                ,yes: submitFuc
            });
    };

    var deleFuc=function(ids){
        layer.confirm('如果有子账号同子账号会一同删除，请谨慎操作，确定删除该用户吗？', function(index) {
            //执行 Ajax 后重载
            DianJinAjax("1",{dataCode: "",resCode: ""},{
                url:'/admin/service/del',
                type:'get',
                data:{ids:ids},
                success:function(result){
                    layer.close(index);
                    layer.msg(result.msg)
                    if(result.code==200){
                        table.reload(table_elem_id);
                    }

                }});

        });
    }
 var stateFuc=function(ids,state=9){
            layer.confirm('确定'+(state==9?'停用':'启用')+'吗？', function(index) {
                //执行 Ajax 后重载
                DianJinAjax("1",{dataCode: "",resCode: ""},{
                    url:'/admin/service/upstate',
                    type:'get',
                    data:{ids:ids,state:state},
                    success:function(result){
                        layer.close(index);
                        layer.msg(result.msg)
                        if(result.code==200){
                            table.reload(table_elem_id);
                        }

                    }});

            });
        }
 var infoFuc=function(id){
     layer.open({
                type: 2
                ,title: '详情'
                ,content: './info?id='+id
                ,maxmin: true
                ,area: layer_open_area(['100%', '100%'])
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    layer.close(index); //关闭弹层
                }
            });
    };
        //监听行工具事件
    table.on('tool('+table_elem_id+')', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            ,layEvent = obj.event; //获得 lay-event 对应的值

        if(layEvent === 'info'){
           infoFuc(data.ServiceId);
        } else if(layEvent === 'del'){
            stateFuc(data.ServiceId,data.State==9?0:9);
        }else if(layEvent === 'edit'){
            edit(data.ServiceId);
        }else if(layEvent === 'webadd'){
            edit(data.ServiceId);
        }else if(layEvent === 'addkfx2'){
            add("type3",data.ServiceId);
        }else if(layEvent === 'deleuser'){
            deleFuc(data.ServiceId);
        }else if(layEvent === 'addOtime'){
            addOtimeFuc(data);
        }

    });






    //监听排序
     table.on('sort('+table_elem_id+')', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        console.log(obj.field); //当前排序的字段名
        console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
        console.log(this) //当前排序的 th 对象*/

        return false;
    });




     //头工具栏事件
  table.on('toolbar('+table_elem_id+')', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'delAll':
         var checkStatus = table.checkStatus(table_elem_id)
        ,checkData = checkStatus.data; //得到选中的数据

        if(checkData.length === 0){
          return layer.msg('请选择数据');
        }
        var ids=[];
        for(var i in checkData){
            ids.push(checkData[i].ServiceId);
        }
        deleFuc(ids.join(','));
      break;
      case 'add':
        add();
      break;
      //自定义头工具栏右侧图标 - 提示
      case 'ShowSearch':
      var display=$("#searchBox").css('display');
       $("#searchBox").css('display',display=='block'?'none':'block');
      break;
    };
  });





})