var boss_id=getQueryString('boss_id');

var vueapp = new Vue({
    el: '#vueapp',
    data: {
        warnList: [],
        limit:15,
        page:1,
        boss_id:getQueryString("boss_id"),

    }, mounted: function () {

    },
    created: function () {
        var _this = this;

        _this.getwarnList();

    },
    methods: {
        ticketContent(ticketContentstr){
            var strobj=JSON.parse(ticketContentstr);
            return strobj.content
        },
        getwarnList(page) {
            var _this=this;
            if (page){
                _this.page=page;
            }
            DianJinAjax(1, {dataCode: false, resCode: false}, {
                url: '/admin/api/darnlist',
                type: "get",
                data: {
                    "boss_id":_this.boss_id,
                    "page":_this.page,
                    "limit":_this.limit,
                },
                success: function (res) {
                    if (res.code == 200) {
                        _this.warnList=res.data.data;

                        if (_this.page==1){
                            layui.use(function(){
                                var laypage = layui.laypage;
                                // 普通用法
                                laypage.render({
                                    elem: 'demo-laypage-pn-show',
                                    count: res.data.page.Total, // 数据总数
                                    limit: _this.limit,
                                    jump: function(obj, first){
                                        // 首次不执行
                                        if(!first){
                                            _this.getwarnList(obj.curr);
                                            //layer.msg('第 '+ obj.curr +' 页');
                                        }
                                    }
                                });

                            });
                        }


                    }
                    //layer.msg(res.msg);
                },
                error: function (data) {


                }
            });
        },edit(warnmsg){
            var url="/admin/warn/info?id="+warnmsg.Id;
            var title="消息详情";
            if(warnmsg.Type=="tickets"){
                url="/admin/tickets/from/update?no="+warnmsg.Linkno;
                title="工单处理";
            }

            layer.open({
                type: 2
                ,title: title
                ,content: url
                ,maxmin: true
                ,area: layer_open_area(['90%', '90%'])
                ,btn: [ '新窗口打开','关闭']
                ,yes:function () {
                    window.open(url);
                },btn2: function (index) {
                    layer.close(index); //关闭弹层
                    DianJinAjax("1",{resCode: true,dataCode: false},{
                        url: '/admin/api/darnread',
                        type: 'POST',
                        data: {ids:warnmsg.Id},
                        success: function (result) {
                            vueapp.getwarnList();
                        }
                    });

                }

            });
        }, deleFuc(ids){
            layer.confirm('确定删除吗？', function(index) {
                //执行 Ajax 后重载
                DianJinAjax("1",{resCode: true,dataCode: false},{
                    url:'/admin/api/darndell',
                    type:'POST',
                    data:{ids:ids},
                    success:function(result){
                        layer.close(index);
                        layer.msg(result.msg)
                        if(result.code==200){
                            vueapp.getwarnList();
                        }

                    }});

            });
        }
    }
})










