/*
var vueapp = new Vue({
    el: '#vueapp',
    data: {
        btns1: [],
        btns2: [],

    }, mounted: function () {

    },
    methods: {
        addbnt1(){
            this.btns1.push({
                remark:"",
                domain:"",
            });
        },addbnt2(){
            this.btns2.push({
                remark:"",
                domain:"",
            });
        }, delebnt1(index){
            this.btns1.splice(index,1);
        }, delebnt2(index){
            this.btns2.splice(index,1);
        },submitvue(){
            $("#black").val(JSON.stringify(this.btns1));
            $("#white").val(JSON.stringify(this.btns2));
        }
    }
})*/
var  configjson={
    "Title":"域名黑白名单",
    "Cvalue":{},
    "Isjson":1,
    "Ckey":"setdomainwhite",
    "Remark":"",
}

var $, form;
var upload;
var configkey = configjson.Ckey;

layui.config({
    base: '../../public/style_js_admin/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'upload', 'form'], function(){
    $ = layui.jquery
        ,form = layui.form;
    upload = layui.upload;



    if(configkey){
        layer.load(1)

        DianJinAjax("1",{dataCode: false,resCode: false},{
            url: '/admin/configget',
            type: 'GET',
            data: {configkey: configkey},
            success: function (result) {
                if (result.code == 200) {
                    //layuiadmin-form-role 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                    //给表单赋值
                    if(result.data.Cvalue!=""){
                        var jsonk=JSON.parse(result.data.Cvalue);
                        if(jsonk){
                            form.val("layuiadmin-form-role",jsonk);
                          /*  if(jsonk.black!=""){
                                vueapp.btns1=JSON.parse(jsonk.black);
                            }
                            if(jsonk.white!=""){
                                vueapp.btns2=JSON.parse(jsonk.white);
                            }*/
                        }
                    }


                    //


                    /*
                    $("#logoimg").attr("src",result.data.Logo);
                    jQuery('#summernote').summernote('code', result.data.KefuGonggao);*/
                } else {
                    layer.msg(result.message)
                }
                layer.closeAll();
            }
        });
    }

    form.on('submit(LAY-user-role-submit)', function(data){
        var field = data.field; //获取提交的字段

        configjson.Cvalue=JSON.stringify(field);
        //提交 Ajax 成功后，静态更新表格中的数据
        DianJinAjax("1",{dataCode: true,resCode: false},{
            url: '/admin/configset',
            type:'POST',
            data:configjson,
            success:function(result){
                layer.msg(result.msg)
                if(result.code==200){
                    setTimeout(function(){
                        window.location.reload();
                    },1000)
                }

            }});

    });


    //同时绑定多个元素，并将属性设定在元素上
    upload.render({
        elem: '.demoMoref'
        ,url:'/api/uploadimg'
        ,data:{name:"logologin",path:"config/images/webset/"}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#loginlogo").val(res.data);
            $("#loginlogoimg").attr("src",res.data);
        }
    })

    upload.render({
        elem: '.demoMore'
        ,url:'/api/uploadimg'
        ,data:{name:"logo",path:"config/images/webset/"}
        ,before: function(){
            //layer.tips('接口地址：'+ this.url, this.item, {tips: 1});
        }
        ,done: function(res, index, upload){
            if(res.code!=200){
                layer.alert(res.msg)
                return
            }
            $("#logo").val(res.data);
            $("#logoimg").attr("src",res.data);

        }
    })

});