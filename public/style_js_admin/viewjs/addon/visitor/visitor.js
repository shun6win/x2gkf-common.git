window.isWindow=getCookie("isWindow");//是否客户端
//发送消息给客户端
function windowEsendMsg(content) {
    console.log(content);
    if(window.isWindow){
        //这里要改变时间，否则客户端不会改变
        document.title=new Date().getTime()+"@"+content;
    }
}

var vueapp=new Vue({
    el: '#app',
    data: {
        conn:{},
        msgList:{},
        msgListPage:{},
        bossList:[],
        nureadList:[],//未读消息对象
        visitorList:[],
        screenshot:window.isWindow,
        ToId:"",//訪客id
        token:getCookie("Authorization"),
        visitor_head:SERVICEHEAD,//客服头像
        service_nick:SERVICENICK,//客服昵称
        yuyanlist:{},//语言列表
        boss_id:1,//客服id
        service_id:SERVICEID,//客服id
        quickmsgList:[],//快捷回复信息
        showdeleQuick:false,//显示快捷回复删除按钮
        showShowquick:false,
        warnreadnum:0,//预警信息数量
        warnmsgList:[],// 推送的通知消息
        ticketsreadnum:0,// 工单信息数量

        visitorselecttype:0,//商家访客状态选择
        AddfriendsBox:false,//添加好友box
        selecthyinput:'',
        selecthylist:[],
        Otime:{
          isov:true,
          time:""
        },
        visitorlog:{},
        showbossbox:false,
        visitorinfo:{
            "Vid": 0,
            "VisiterId": "",
            "ToId": 0,
            "VisiterName": "",
            "Channel": "",
            "Avatar": "",
            "Name": "",
            "Tel": "",
            "LoginNum": 0,
            "Connect": "",
            "Remark": "",
            "Extends": {
                "language": "",
                "userAgent": "",
                "os": {
                    "os": "",
                    "device": ""
                },
                "browserName": ""
            },
            "IpInfo": " ",
            "Ip": "",
            "FromUrl": "",
            "MsgTime": "",
            "LastMsg": "",
            "LastTime": "",
            "BossId": 0,
            "State": 0,
            "Zdy1": "",
            "Zdy2": ""
        },//客服id
        bossInfo:{
            "Id": -1,
            "BossName": "",
            "AdminId": 0,
            "Logo": "",
            "Copyright": "",
            "VideoState": 1,
            "VoiceState": -1,
            "AudioState": 0,
            "TemplateState": 0,
            "DistributionRule": 0,
            "VoiceAddress": "",
            "Remark": "",
            "OverTime": 0,
            "MaxCount": 0,
            "PushUrl": "",
            "Status": -1
        },
        quickBossInfo:{},
        serviceinfo:{Worktimejson:""},
        showUserInput:'',
        limit:15,
        MsgNoAname:'',//消息定位用
        showIconBtns:false,
        messageContent:"",
        messageContent2:"",
        sendDisabled:false,
        showExtendTool:false,
        showLanguage:false,
        languagetype: Language,
        languageList: [
            {"title": "简体中文", "type": "zh"},
            {"title": "English", "type": "en"}
        ],
        sendFaceIconList:[],
        chatBoxSendCloseShow:false,
        VisitorVoiceBtn:'false',
        VisitorMapBtn:'',
        VisitorCommentBtn:'',
        showFaceIcon:false,
        VisitorReadStatus:'true',
        VisitorPlusBtn:'false',
        VisitorUploadImgBtn:'false',
        VisitorUploadFileBtn:'false',
        VisitorMaxLength:'100'==''?100:parseInt('100'),
        VisitorShowAvator:'false',
        sendPingTime:30,
        sendPingTimeDef:30,

    },mounted: function (){

    },
    created: function () {
        var _this=this;
        _this.quickBossInfo=_this.bossInfo;
        _this.connIni(_this.service_id);
        _this.apibosslist();
        _this.apiquickmsglist();
        _this.getyuyanlist();
        _this.getServerinfo();
        // window 每次获得焦点
        window.onfocus = function() {
            _this.MsgReadFunc();
        };
        setInterval(function (){
            _this.sendPingTime++;
            if(_this.sendPingTime%_this.sendPingTimeDef==0){
                //_this.sendPingTime = _this.sendPingTimeDef;
                _this.sendPing();
            }

            if(_this.sendPingTime%60==0){
                _this.getServerinfo();
            }
        },1000)
        _this.warnFuncreadnum();
        _this.ticketsFuncreadnum();

    },
    methods:{
        deleVisitor(visitor_id){
            layer.confirm('删除后，会直接删除访客信息和聊天记录，确定删除吗？', function(index) {
                //执行 Ajax 后重载
                DianJinAjax(1,{dataCode: false,resCode: false}, {
                    url: '/admin/visitor/del',
                    data:{visitor_id:visitor_id,ismsg:1},
                    type: "get",
                    success: function (res) {
                        if(res.code==200){
                            setTimeout(function () {
                                window.location.reload();
                            },1000);
                        }
                        layer.alert(res.msg);
                    }
                });


            });
        },
        getServerinfo(){
            var _this=this;
            DianJinAjax(1,{dataCode: false,resCode: false}, {
                url: '/admin/api/service/myinfo',
                data:{},
                type: "get",
                success: function (res) {
                    if(res.code==200){
                        _this.serviceinfo=res.data;
                        if (_this.serviceinfo.Otype>0 && _this.serviceinfo.Otime){
                            var newtime=Date.parse(new Date())/1000
                            _this.Otime.time=_this.serviceinfo.Otime==1999999999?'':formatDateTime(_this.serviceinfo.Otime)
                            _this.Otime.isov=newtime-_this.serviceinfo.Otime+172800>0;
                            if(newtime-_this.serviceinfo.Otime>0){
                                //显示自动关闭倒计秒数
                                layer.alert('账号已到期！', {
                                    time: 15*1000
                                    ,skin: 'layui-layer-molv' //样式类名
                                    ,success: function(layero, index){
                                        var timeNum = this.time/1000, setText = function(start){
                                            layer.title((start ? timeNum : --timeNum) + ' 秒后自动退出', index);
                                        };
                                        setText(!0);
                                        this.timer = setInterval(setText, 1000);
                                        if(timeNum <= 0) {
                                            clearInterval(this.timer)
                                            window.location.href="/service/login";
                                        };
                                    }
                                    ,end: function(){
                                        clearInterval(this.timer);
                                        window.location.href="/service/login";
                                    }
                                });
                                //console.log(newtime,_this.serviceinfo.Otime,newtime-_this.serviceinfo.Otime)
                                //
                            }
                        }
                    }

                }
            });
        },
        OtimeAdd(){
            var url="https://kefu.x2gkf.com/v/JLkbbDuFttJvKJm8zukYRw==?v_id=x2g:"+this.serviceinfo.ServiceId+"&v_name=客服用户ID:"+this.serviceinfo.ServiceId+"&zdy1="+this.serviceinfo.ServiceId

            layer.open({
            type: 2
            , title: '联系续费：客服'
            , content: url
            , maxmin: true
            , area: ['80%', '80%']
                ,btn: ['新窗口打开']
            ,yes:function (index,ink) {
                layer.close(index);
                window.open(url);
            }
        })
        },
        MsgReadFunc(){//标记消息已读

            var _this=this;
            if(_this.visitorinfo.VisiterId==""){
                return
            }
            var _mkey = msgMkey(_this.ToId,_this.service_id);
            setTimeout(function (){
                if((!document.hidden)
                    || !document.getElementById(_mkey).style
                    || !document.getElementById(_mkey).style.display
                    || document.getElementById(_mkey).style.display!="none"
                ){//当前屏幕
                   var _mkeyindex=_this.findIndexVisiter(_this.visitorinfo.BossId,_mkey);
                    if(
                        _this.visitorList[_this.visitorinfo.BossId] &&
                        _this.visitorList[_this.visitorinfo.BossId][_mkeyindex]
                        &&  _this.visitorList[_this.visitorinfo.BossId][_mkeyindex].Isread>0
                    ){
                        for (var k in _this.msgList[_mkey]){
                            if(_this.msgList[_mkey][k].ToId!=_this.service_id){
                                _this.msgList[_mkey][k].Isread=1;
                            }

                        }
                        _this.sendInputingStrNow("1","text","read");
                        _this.MsgReadNumFunc(_this.visitorinfo.BossId,_this.visitorinfo.VisiterId,0);
                    }
                }

            },100)
        },
        MsgReadNumFunc(bossId,VisiterId,num){
           // console.log("MsgReadNumFunc---------start-----",bossId,VisiterId,num)
            var _this=this;
            if(!_this.bossList[bossId]){
                return;
            }
            var _mkey = msgMkey(VisiterId,_this.service_id);
            var _mkeyindex=_this.findIndexVisiter(bossId,_mkey);
            var _mk=_this.visitorList[bossId]?(_this.visitorList[bossId][_mkeyindex]?1:0):0;

            var VisiterIdnum=_mk?_this.visitorList[bossId][_mkeyindex].Isread:0;
            if(num==0){
                _this.bossList[bossId].Isread-=VisiterIdnum;
                if(_mk)_this.visitorList[bossId][_mkeyindex].Isread=num;
            }else{
                _this.bossList[bossId].Isread+=num;
                if(_mk)_this.visitorList[bossId][_mkeyindex].Isread+=num;
            }
            var znum=0;
            for (var i in _this.bossList){
                znum+= _this.bossList[i].Isread;
            }
            _this.msgNotifyFunc(znum);
            _this.$forceUpdate();
            windowEsendMsg("newmsgnum=>"+znum);

        },
        selectShowQuick(bossinfo){
            this.quickBossInfo=bossinfo;
            this.showShowquick=!this.showShowquick

        },
        selectVisitor(Visitorinfod){

            var _this=this;
            if(Visitorinfod.VisiterId!=_this.ToId) {
                _this.ToId = Visitorinfod.VisiterId;
                _this.visitorinfo.VisiterId = Visitorinfod.VisiterId;
                _this.boss_id=_this.bossInfo.Id;
                _this.apigetmsg(Visitorinfod.VisiterId);

                _this.apivisitorinfo();
                _this.apiVisitorlog();
            }
            _this.MsgReadFunc();
            _this.scrollBottom();
            setTimeout(function () {
                _this.scrollBottom();
            },500)
        },
        selectBoss(boss_info,istrue=false){
            var _this=this;
            if(boss_info.Id!=_this.bossInfo.Id || istrue){
                _this.bossInfo=boss_info
                _this.quickBossInfo=_this.bossInfo;
                _this.apivisitorlist(boss_info.Id);
                setCookie("selectBoss", JSON.stringify(boss_info));
            }


        },
        connIni(service_id){

            var _this=this;
            _this.service_id=service_id
            setCookie("service_id", _this.service_id);


            if (window["WebSocket"]) {
                _this.conn = new WebSocket(getWsUrl()+"/ws?Authorization="+_this.token);

                _this.conn.onopen = _this.OnOpen;
                _this.conn.onclose = _this.OnClose;
                _this.conn.onmessage =_this.OnMessage;
            } else {
                console.log("Your browser does not support WebSockets");

            }
        },
        OnOpen:function() {
            this.chatBoxSendCloseShow=false;

        },
        OnMessage:function(evt) {
            if(evt.data=='{"T":"po"}'){return;}
            var _this=this;
            var messages = JSON.parse(evt.data);
            var OnMessageVisiterId=messages.D.ToId==_this.service_id?messages.D.FromId:messages.D.ToId;
            var _mkey=msgMkey(messages.D.ToId,messages.D.FromId);

            _this.sendPingTime = _this.sendPingTimeDef;
            //预警消息
            if(messages.T=="warn"){
                _this.warnreadnum= _this.warnreadnum+1;
                if(_this.serviceinfo.Worktimejson==""){
                    _this.msgNotifyFunc("通知"+_this.warnreadnum,true);
                    var contentObj=JSON.parse(DianJin_decode(messages.D.Content,messages.D.BossId));
                    contentObj.Isshow=1;
                    _this.warnmsgList.push(contentObj);

                }
                return;
            }

            //没有商家 访客
            if(!_this.visitorList[messages.D.BossId]){
                if(messages.T=="msg" && messages.D.Isread==0) {
                    _this.MsgReadNumFunc(messages.D.BossId, messages.D.FromId, 1);
                }
                return;
            }
            var _mkeyindex=_this.findIndexVisiter(messages.D.BossId,_mkey);
            if(_mkeyindex==-1){
                _mkeyindex=_this.visitorList[messages.D.BossId].length-1;
                _this.visitorList[messages.D.BossId].push(
                    {
                        mkey: _mkey,
                        VisiterId: OnMessageVisiterId,
                        VisiterName: messages.D.Fnick,
                        Avatar: messages.D.Fhead,
                        BossId: messages.D.BossId,
                        LastMsg: messages.D.Content,
                        MsgTime: messages.D.Ftime,
                        State: 1,
                        Isread: 0
                    }
                );
                //如果是好友临时消息，直接加好友
                if(messages.D.BossId==0 && OnMessageVisiterId>0){
                    _this.selectHySendFun(OnMessageVisiterId,false);
                }
            }



            if(messages.T=="msg"){
                _this.showUserInput = '';
                if(_this.msgList[_mkey]){
                    _this.msgList[_mkey].push(messages.D);
                }
                if(messages.D.FromId==_this.ToId || messages.D.ToId==_this.ToId){
                    buildImagelookBigImg("#V"+msgMkey(_this.ToId,_this.service_id)+' .chatMsgContent');
                }
                if(!_this.visitorList[messages.D.BossId]){

                }else{
                    _this.visitorList[messages.D.BossId][_mkeyindex].LastMsg=messages.D.Content;
                    _this.visitorList[messages.D.BossId][_mkeyindex].MsgTime=messages.D.Ftime;
                    //标记访客是否需要回复
                    _this.visitorList[messages.D.BossId][_mkeyindex].LastMsgfrom=messages.D.FromId;

                    //_this.visitorList[messages.D.BossId][OnMessageVisiterId].Isread+=1;
                }

                _this.visitorList[messages.D.BossId].sort(function (a,b) {
                    return  new Date(b.MsgTime).getTime()-new Date(a.MsgTime).getTime()
                })
                _this.$forceUpdate();
                _this.scrollBottom();
                // _this.serviceinfo.Worktimejson=="" 说明是工作时间
                if(messages.D.FromId!=_this.service_id  && messages.D.Isread==0 && _this.serviceinfo.Worktimejson==""){
                    _this.MsgReadNumFunc(messages.D.BossId,messages.D.FromId,1);
                    _this.msgNotifyFunc(0,true);
                    //发送消息给pc端
                    var messagespc = JSON.parse(evt.data);
                    messagespc.D.Fnick=DianJin_encode(messagespc.D.Fnick,messagespc.D.BossId);
                    messagespc.D.BossName="";
                    if( _this.bossList[messagespc.D.BossId]){
                        messagespc.D.BossName=DianJin_encode(_this.bossList[messagespc.D.BossId].BossName,messagespc.D.BossId);;
                    }
                    windowEsendMsg("newmsg=>"+JSON.stringify(messagespc)+"<=");
                }



            }else if(messages.T=="recall"){//消息测回
                var reindex = -1;
                for ( index in _this.msgList[_mkey]){
                    if(_this.msgList[_mkey][index].MsgNo==messages.D.Content){
                        reindex=index;
                        break
                    }
                }
                if (reindex>-1){
                    _this.msgList[_mkey].splice(reindex, 1);
                    _this.$forceUpdate();
                }

            }else if(messages.T=="read") {//消息已读
                for ( index in _this.msgList[_mkey]){
                    if( _this.msgList[_mkey][index].FromId==_this.service_id){
                        _this.msgList[_mkey][index].Isread=1;
                    }

                }
                _this.$forceUpdate();
            }else if(messages.T=="login") {//访客登入
                if(!_this.visitorList[messages.D.BossId][_mkeyindex]){
                    _this.visitorList[messages.D.BossId][_mkeyindex]={};
                }
                _this.visitorList[messages.D.BossId][_mkeyindex].State=1;
                _this.visitorList[messages.D.BossId][_mkeyindex].LastMsg=messages.D.Content;
                _this.visitorList[messages.D.BossId][_mkeyindex].MsgTime=messages.D.Ftime;
                if(_this.visitorinfo.VisiterId==messages.D.FromId){
                    _this.visitorinfo.State=1;
                    _this.visitorinfo.LastTime=messages.D.Ftime;
                }
                _this.$forceUpdate();
            }else if(messages.T=="logout") {//访客退出
                _this.visitorList[messages.D.BossId][_mkeyindex].State=-1;
                _this.visitorList[messages.D.BossId][_mkeyindex].LastMsg=DianJin_encode("访客离开",messages.D.BossId);
                _this.visitorList[messages.D.BossId][_mkeyindex].MsgTime=messages.D.Ftime;
                if(_this.visitorinfo.VisiterId==messages.D.FromId){
                    _this.visitorinfo.State=-1;
                    _this.visitorinfo.LastTime=messages.D.Ftime;
                }
                _this.$forceUpdate();
            }else if(messages.T=="input"){
                if(_this.visitorinfo.VisiterId== messages.D.FromId
                    || (_this.visitorinfo.VisiterId== messages.D.ToId && messages.D.Fnick!=_this.service_nick)
                ){
                    _this.showUserInput = "["+messages.D.Fnick+"]输入中…："+DianJin_decode(messages.D.Content,messages.D.BossId);
                }
            }



        },
        OnClose:function(evt) {
            var _this=this;
            _this.chatBoxSendCloseShow=true;
          setTimeout(function () {
                  _this.connIni(_this.service_id);

            },5000)
        },
        findIndexVisiter(bossid,mkey){
            var _this=this;
            if(!_this.visitorList[bossid])return -1;
            for (var i=0;i<_this.visitorList[bossid].length;i++){
                if(_this.visitorList[bossid][i].mkey==mkey){
                    return i;
                }
            }
            return -1;

        },
        sendPing(){
            if(this.chatBoxSendCloseShow){
                return;
            }
            this.conn.send('{"T":"pi"}');
        },
        MsgTp(toid){
            return {
                "T": "msg",
                "D": {
                    "MsgType": "text",
                    "MsgNo": "1",
                    "Mkey": this.service_id+"|"+this.ToId,
                    "FromId": this.service_id+"",
                    "ToId": (toid?toid:this.ToId)+"",
                    "BossId": this.boss_id-0,
                    "Content": "",
                    "Content2": "",
                    "Fhead":this.visitor_head,
                    "Fnick": this.service_nick,
                    "Ftime": "",
                    "Isread": 0,
                    "Extend": ""
                }
            };
        },
        msgNotifyFunc(num,newtitle=false){
            if(newtitle){

                notify.player();
            }else{
                notify.setFavicon(num)
            }


        },
        chatToUser:function(e) {

            if(this.sendDisabled || this.messageContent==""){
                return;
            }

            if(e && e.ctrlKey && e.keyCode==13) {
                this.messageContent += '\n';
                return;
            }
            if(e && e.keyCode==13 && this.messageContent.slice(-1)=="\n") {
                //去除回车自动加的\n
                this.messageContent=this.messageContent.toString().slice(0,this.messageContent.length-1);
            }

            this.sendInputingStrNow(SendTextEncode(this.messageContent),'text')
            this.messageContent="";
            this.messageContent2=""
            this.showFaceIcon=false;
        },
        sendFaceIconListb(){
            var _this=this;
            _this.showFaceIcon=!_this.showFaceIcon;
            if(_this.sendFaceIconList.length>0)return;
            for(var i=0;i<43;i++){
                _this.sendFaceIconList.push('/public/style_js_index/image/faces/'+i+'.png')
            }
        },
        sendFaceIcon(facetext){
            this.messageContent+=facetext
        },
        inputNextText:function(e){

            if(!this.conn || this.messageContent=="" || this.messageContent=="\n"){
                return;
            }
            var _this=this;
            _this.sendDisabled = false;
            this.messageContent2=""
            _this.sendInputingStrNow(SendTextEncode(this.messageContent),'text','input');
        },
        recallMsg(MsgNo){
            var message = this.MsgTp();
            message.T='recall';
            message.D.Content =MsgNo;
            message.D.MsgType ='text';
            this.conn.send(JSON.stringify(message));
        },
        sendInputingStrNow:function(str,msgtype="text",messageT="msg",Isread=0){
            if(!this.conn || str=="" || str=="\n" || this.visitorinfo.BossId<0 || this.ToId<0){
                return;
            }
            this.sendPingTime = this.sendPingTimeDef;
            var message = this.MsgTp();

            message.T=messageT;
            message.D.Content =DianJin_encode(str,this.visitorinfo.BossId);
            message.D.Content2 =DianJin_encode(this.messageContent2,this.visitorinfo.BossId);
            message.D.MsgType =msgtype;
            message.D.Isread =Isread;
            this.conn.send(JSON.stringify(message));
        },
        sendInputingStrNowPc:function(str,msgtype="text",messageT="msg",toid="",Bossid=0){
            Bossid=Bossid-0;
            if(!this.conn || str=="" || str=="\n" || Bossid<0){
                return;
            }
            this.sendPingTime = this.sendPingTimeDef;
            var message = this.MsgTp(toid);
            message.T=messageT;
            message.D.BossId=Bossid;
            message.D.Content =str;//DianJin_encode(str,Bossid);
            message.D.Content2 =DianJin_encode(this.messageContent2,Bossid);
            message.D.MsgType =msgtype;
            this.conn.send(JSON.stringify(message));
        },
        setLanguage(type){

            this.languagetype=type;
        },
        scrollBottom:function(){
            var _this=this;
            _this.$nextTick(function(){
                var container = _this.$el.querySelector(".chatVisitorPage");
                container.scrollTop = 99999;
            });


        },
        TranslateTy(){
            var _this=this;
            if(_this.messageContent==""){
                return;
            }
            apiTranslateTy(_this.messageContent,function (textt) {
                _this.messageContent2=textt;
            },"auto_"+_this.visitorinfo.Language);
        },
        getyuyanlist(){
            var _this=this;
            $.ajax({
                url: '/public/style_js_index/fanyiconfig.json',
                type: "get",
                data: {},
                success: function (res) {
                    _this.languageList=res;
                    if(_this.languagetype=="" || !_this.languageList[_this.languagetype]){
                        layer.alert("客服语言标识【"+_this.languagetype+"】不存在，请在右上角客服昵称下拉修改基础资料中修改语言，现已为您切换到默认【简体中文 zh】")
                        _this.languagetype='zh';
                    }
                }
            });
        },
        languageSelect(event){
            this.visitorinfo.Language=event.target.value;
            DianJinAjax(1,{dataCode: false,resCode: false},{
                url: '/api/visitor/language',
                type: "post",
                data: {"visitor_id":this.visitorinfo.VisiterId,"language":event.target.value},
                success: function (res) {
                    layer.alert('该访客语言设置成功',{time:1000});
                    //layer.tips("该访客语言设置成功")
                }
            });

        },
        TranslateTyMsg(msg){
            var messageContent=msgtextFace(DianJin_decode(msg.Content,msg.BossId));
            if(messageContent==""){
                return;
            }
            var _this=this;
            apiTranslateTy(messageContent,function (textt) {
                $("#"+msg.MsgNo+'content2').html('<div style="background: #67c23a59">'+textt+'</div>');
                DianJinAjax(1,{dataCode: true,resCode: true},{
                    url: '/admin/api/msgupcontent2',
                    type: "post",
                    data: {"text":DianJin_encode(textt,msg.BossId),"msgno":msg.MsgNo},
                    success: function (res) {
                    }
                });

            },"auto_"+_this.languagetype);
        },
        textareaFocus:function(){
            this.scrollBottom();
        },
        textareaBlur:function(){
            this.scrollBottom();
        },
        faceIconClick:function(index){
            // this.showFaceIcon=false;
            this.messageContent+="face"+this.face[index].name;
        },
        selectQuick(msg,isstring=false) {
            if (isstring){
                var _dj=DianJin_decode(msg,"msg");
                //_dj=HTMLDecode(_dj);
                msg=JSON.parse(_dj);
            }
          var _this=this;
            // 新页面打开

                layer.open({
                    title:msg.Mtype==2?"常见问题❔":"快捷回复",
                    type:1,
                    area: layer_open_area(['80%', '80%']), //宽高
                    content:'<div style="padding: 5px"><h2 style="padding: 10px;text-align: center;">'+msg.Title+'<hr></h2>'+msg.Content+'</div>',
                    btn:["发送","✖️"],
                    yes: function ( index) {
                        layer.close(index);
                        _this.sendInputingStrNow(msg.Content,'text','msg');
                    },
                    btn2: function (layero, index) {

                    }
                });
        },
        apivisitorlist(boss_id,issx=false){
            var _this=this;

            if(!(!_this.visitorList[boss_id] || JSON.stringify(_this.visitorList[boss_id])=='[]' )  && !issx){
                return;
            }

            DianJinAjax(boss_id,{dataCode: false,resCode: true},{
                url: '/admin/api/visitorall',
                type: "get",
                data: {"boss_id":boss_id,"service_id":_this.service_id},
                success: function (res) {

                    if(res.code==200){
                        boss_id = boss_id - 0;

                        _this.visitorList[boss_id] = [];
                        var _debf=0;
                        for (var idk in res.data){
                            var di=res.data[idk];
                            di.mkey=msgMkey(di.ToId,di.VisiterId);
                            di.Isread=_this.nureadList[di.mkey]?_this.nureadList[di.mkey]:0;
                            _this.visitorList[boss_id].push(di);
                            if(_debf==0){
                                _debf=di;
                            }
                        }

                        _this.$forceUpdate();
                       /* setTimeout(function () {
                            iniLayui()
                           /!* if(_this.visitorinfo.VisiterId=="" && _debf!=0){
                                _this.selectVisitor(_debf);
                            }*!/
                        },200)*/
                    }

                }
            });
        },
        apibosslist(){
            var _this=this;
            DianJinAjax(_this.boss_id,{dataCode: false,resCode: true},{
                url: '/admin/api/serviceboss',
                type: "get",
                data: {"service_id":_this.service_id},
                success: function (res) {
                    if(res.code==200){
                        var _bossList={};
                        var selectBosscook= getCookie("selectBoss");
                       if(selectBosscook && (selectBosscook+"").length>5){
                           selectBosscook=JSON.parse(selectBosscook);
                       }
                        for (var i in res.data.bosslist){
                            var kread=res.data.bosslist[i];
                            kread.Isread=0;
                            if(kread.Btnjson!=""){
                                kread.Btnjson=JSON.parse(kread.Btnjson);
                            }
                            _bossList[kread.Id]=kread;
                            if(selectBosscook.Id && selectBosscook.Id==res.data['bosslist'][i].Id){
                                _this.selectBoss(res.data['bosslist'][i],true)
                            }


                        }
                        if(_this.bossInfo.Id-0<0 && res.data['bosslist'].length>0){
                            _this.selectBoss(res.data['bosslist'][i],true)
                        }
                        _this.bossList=_bossList;

                        for (var i in res.data.nureadlist){
                            var kread=res.data.nureadlist[i];
                            kread.BossId=kread.BossId+"";
                            if(_this.bossList[kread.BossId]){
                                _this.bossList[kread.BossId].Isread+=kread.Total;
                            }
                            _this.nureadList[kread.Mkey]=kread.Total;
                        }


                        setTimeout(iniLayui,200)
                    }

                }
            });
        },apiquickmsglist(){
            var _this=this;
            DianJinAjax(_this.boss_id,{dataCode: false,resCode: true},{
                url: '/admin/api/quickmsg',
                type: "get",
                data: {"service_id":_this.service_id},
                success: function (res) {
                    if(res.code==200){
                        _this.quickmsgList=res.data;
                        setTimeout(function () {
                            iniLayui();
                            $('*[lay-tips]').on('mouseenter', function(){
                                var content = $(this).attr('lay-tips');

                                this.index = layer.tips('<div style="padding: 10px; font-size: 14px; color: #eee;">'+ content + '</div>', this, {
                                    time: -1
                                    ,maxWidth: 280
                                    ,tips: [3, '#3A3D49']
                                });
                            }).on('mouseleave', function(){
                                layer.close(this.index);
                            });
                        },200)
                    }

                }
            });
        },
        btnTeplace(titleurl){
            visitorinfo=this.visitorinfo;
            var zdyvids=visitorinfo.VisiterId.split("z-");
          return   titleurl.replace("[v_id]",zdyvids.length>1?zdyvids[1]:zdyvids[0]).replace("[v_name]",visitorinfo.Name).replace("[ip]",visitorinfo.Ip).replace("[boss_id]",visitorinfo.BossId).replace("[zdy1]",visitorinfo.Zdy1).replace("[zdy2]",visitorinfo.Zdy2);
        },
        apivisitorinfo(){
            var _this=this;
            DianJinAjax(_this.boss_id,{dataCode: false,resCode: true},{
                url: '/admin/api/visitorinfo',
                type: "get",
                data: {"boss_id":_this.boss_id,"visitor_id":_this.ToId},
                success: function (res) {
                    if(res.code==200){
                        if(res.data.Vid){
                            res.data.Extends=JSON.parse(res.data.Extends)
                            _this.visitorinfo = res.data;
                            //如果客服语言同，访客语言不一致 显示翻译按钮
                            _this.showLanguage=(_this.languagetype!=_this.visitorinfo.Language);
                        }
                    }

                }
            });
        },
        selectHyFun(){
            var _this=this;
            DianJinAjax(_this.boss_id,{dataCode: false,resCode: true},{
                url: '/admin/service/so',
                type: "get",
                data: {"so":_this.selecthyinput},
                success: function (res) {
                    if(res.code==200){
                        _this.selecthylist=res.data.data;
                    }

                }
            });
        },
        selectHySendFun(ServiceId,isselect=true){
            var _this = this;
            //在我的好友列表添加 一条访客记录
            DianJinAjax(_this.boss_id, {dataCode: false, resCode: true}, {
                url: '/admin/service/addhy',
                type: "get",
                data: {
                     "service_id": ServiceId
                },
                success: function (res) {
                    if(isselect){
                        if(res.code==200){
                            _this.selectVisitor({VisiterId:ServiceId}) ;
                            _this.apivisitorlist(_this.boss_id);
                        }else{
                            layer.alert(res.msg);
                        }
                    }


                }
            })


        },
        apigetmsgPage(){
            var _this=this;
            var mkey=msgMkey(_this.visitorinfo.VisiterId,_this.service_id);
            if(!_this.msgListPage[mkey]){
               return;
            }
            if(_this.msgListPage[mkey].LastPage<=_this.msgListPage[mkey].Page){
                return;
            }
            _this.apigetmsg(0,_this.msgListPage[mkey].Page+1);
        },
        apigetmsg(VisiterId=0,msgpage=1){
            var _this=this;
            if(!VisiterId){
                VisiterId=_this.visitorinfo.VisiterId;
            }
            var mkey=msgMkey(VisiterId,_this.service_id);
            if(!_this.msgList[mkey]){
                _this.msgList[mkey] = [];
                _this.msgListPage[mkey]={
                    LastPage:1,
                    Limit:15,
                    Page:1,
                    Total:0
                }
            }else{
                if(msgpage==1){
                    // 选择访客，如果已经加载了记录，就不重新加载了
                    return;
                }
            }
            DianJinAjax(_this.boss_id,{dataCode: false,resCode: true},{
                url: '/api/visitormsg',
                type: "get",
                data: {"from":VisiterId,"to":_this.service_id,page:msgpage,limit:_this.limit,"day":10000},
                success: function (res) {
                    if(res.code==200 && res.data.data.length>0){
                        //VisiterId=1
                        var _lsmsg=[];
                        if (res.data.page.Page!=1){
                            _lsmsg=_this.msgList[mkey];
                        }
                        for (index in res.data.data){
                            _lsmsg.unshift(res.data.data[index])
                        }
                        _this.msgList[mkey]=_lsmsg;
                        _this.msgListPage[mkey] = res.data.page;
                        _this.$forceUpdate();
                        if(res.data.data && res.data.data[0] && res.data.data[0].MsgNo!=''){
                            setTimeout(function () {
                                if(msgpage==1){
                                    _this.scrollBottom();
                                }else{
                                    var aks=res.data.data[0].MsgNo
                                    var el = document.getElementById(aks);
                                    if(el){
                                        el.scrollIntoView({
                                            behavior: 'smooth', // 平滑滚动
                                            block: 'start'      // 滚动到视图的顶部
                                        });
                                    }
                                }


                            },100)
                        }


                    }
                    buildImagelookBigImg("#V"+msgMkey(VisiterId,_this.service_id)+' .chatMsgContent');
                },
                error: function (data) {

                }
            });
        },
        uploadImgAndFilePost(url,file,postfilename="file",type="img"){
            var _this=this;
            var formData = new FormData();
            formData.append(postfilename,file);
            var indexload= layer.load(3);
            $.ajax({
                url: url || '',
                type: "post",
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                mimeType: "multipart/form-data",

                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.upload.onprogress = function(e) {
                        var w = parseInt((e.loaded / e.total) * 100)
                        _this.percentage=w;
                        if(w>=100){
                            _this.percentage=0;
                        }
                    }
                    return xhr
                },
                success: function (res) {
                    layer.close(indexload);
                    if(res.code!=200){

                    }else{
                        _this.sendInputingStrNow(res.data,type)
                        setTimeout(function () {
                            _this.scrollBottom();
                        },2000);
                    }
                },
                error: function (data) {
                    layer.close(indexload);
                }
            });
        },
        uploadImg:function (url){
            let _this=this;
            _this.showExtendTool = false;
            $('#uploadImg').after('<input type="file" accept="image/gif,image/jpeg,image/jpg,image/png" id="uploadImgFile" name="file" style="display:none" >');
            $("#uploadImgFile").click();
            $("#uploadImgFile").change(function (e) {
                var file = $("#uploadImgFile")[0].files[0];
                _this.uploadImgAndFilePost(url,file,"imgfile","img");
            });
        },

        uploadFile:function (url){
            let _this=this;
            _this.showExtendTool = false;
            $('#uploadFile').after('<input type="file"  id="uploadRealFile" name="file2" style="display:none" >');
            $("#uploadRealFile").click();
            $("#uploadRealFile").change(function (e) {
                var file = $("#uploadRealFile")[0].files[0];
                _this.uploadImgAndFilePost(url,file,"file","file");
            });
        },

        saveinfo(){
            var _this=this;
            DianJinAjax(_this.boss_id,{dataCode: true,resCode: false},{
                url: '/admin/api/visitorsave',
                type: "post",
                data: _this.visitorinfo,
                success: function (res) {
                    if(res.code==200){
                        _this.apivisitorlist(_this.visitorinfo.BossId,true);
                    }

                }
            });
        },
        addQuick(_quickBossInfo,qmsg={Qmid:0},mtype=1){
            var _this=this;
            layer.open({
                type: 2
                ,title: (qmsg.Qmid?'修改':'新增')+'【'+_quickBossInfo.BossName+"】"+mtype==1?'快捷回复':'常见问题'
                ,content: '/admin/visitor/addquickmsg?BossId='+_quickBossInfo.Id+"&ServiceId="+_this.service_id+(qmsg.Qmid?'&Qmid='+qmsg.Qmid:'')+"&Mtype="+mtype
                ,area: ['80%', '80%']
                ,maxmin: true
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                var iframeWindow = window['layui-layer-iframe'+ index]
                    ,submitID = 'LAY-user-role-submit'
                    ,submit = layero.find('iframe').contents().find('#'+ submitID)
                    ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
                if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;


                //监听提交
              iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                    var field = data.field; //获取提交的字段
                    var fieldint = (inputtypeints+"").split(",");
                    for (d in field){
                        if (fieldint.indexOf(d)!=-1){
                            field[d]=field[d]-0;
                        }
                    }
                    if(field.Indexc>999999999){
                        layer.alert("排序数值不能大于9999999999");return;
                    }
                  field.Keyword=field.Keyword.replace(/，/g, ',');
                    //提交 Ajax 成功后，静态更新表格中的数据
                    DianJinAjax("1",{dataCode: true,resCode: false},{
                        url:'/admin/api/addquickmsg',
                        type:'POST',
                        data:field,
                        success:function(result){
                            layer.msg(result.msg)
                            if(result.code==200){

                                layer.close(index); //关闭弹层
                                _this.apiquickmsglist();
                            }

                        }});

                });
                    submit.trigger('click');
            }})
        },
        deleQuick(qmsg){
            var _this=this;
            layer.msg('是否要删除：'+qmsg.Title, {
                time: 20000, //20s后自动关闭
                btn: ['删除', '取消']
                ,yes: function(){
                    DianJinAjax(_this.boss_id,{dataCode: false,resCode: true},{
                        url: '/admin/api/delequickmsg',
                        type: "get",
                        data: {"id":qmsg.Qmid},
                        success: function (res) {
                            layer.msg(res.msg)
                            if(res.code==200){

                                layer.closeAll();
                                _this.apiquickmsglist();
                            }

                        }
                    });

                }
            });
        },
        setLinkFunc(){
            var _this=this;
            layer.open({
                type: 2
                ,title: ' 接入方法'
                ,content: '/service/setlink?serviceid='+_this.service_id+'&bossid='+_this.bossInfo.Id
                ,area: ['80%', '80%']
                ,maxmin: true
                ,btn: [ '关闭']
                ,yes: function(index, layero){
                    layer.close(index);
                }})
        },
        setBossFunc(){
            var _this=this;
            layer.open({
                type: 2
                ,title: '商家设置'
                ,content: '/admin/boss/edit?id='+_this.bossInfo.Id
                ,maxmin: true
                ,area: ['80%', '80%']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID)
                        ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
                    if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;
                    //监听提交
                    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                        var field = data.field; //获取提交的字段
                        var fieldint = (inputtypeints+"").split(",");
                        for (d in field){
                            if (fieldint.indexOf(d)!=-1){
                                field[d]=field[d]-0;
                            }
                        }
                        //提交 Ajax 成功后，静态更新表格中的数据
                        DianJinAjax("1",{dataCode: true,resCode: false},{
                            url:'/admin/boss/save',
                            type:'POST',
                            data:field,
                            success:function(result){
                                layer.msg(result.msg)
                                if(result.code==200){
                                    layer.close(index); //关闭弹层
                                }

                            }});

                    });

                    submit.trigger('click');
                }
            });
        },apiVisitorlog(){
            var _this=this;
            DianJinAjax("1",{resCode: true,dataCode: false},{
                url: '/admin/api/visitorlog',
                type: 'get',
                 data: {visiter_id: _this.ToId,boss_id:_this.boss_id},
                success: function (result) {
                    if(result.data && result.data.data){
                        _this.visitorlog=result.data;
                    }else{
                        _this.visitorlog=[];
                    }

                }
            });
        },ticketsFuncreadnum(){
            var _this=this;
            DianJinAjax("1",{resCode: true,dataCode: false},{
                url: '/admin/api/ticketsread0',
                type: 'get',
                success: function (result) {
                    _this.ticketsreadnum=result.data;
                }
            });
        },warnFuncreadnum(){
            var _this=this;
            DianJinAjax("1",{resCode: true,dataCode: false},{
                url: '/admin/api/darnread0',
                type: 'get',
                // data: {ids: _this.id},
                success: function (result) {
                    _this.warnreadnum=result.data;
                }
            });
        },warnFunc(){
            var _this=this;
            _this.warnFuncreadnum();
            layer.open({
                type: 2
                ,title: '我的消息'
                ,content: '/admin/warn/?boss_id='+_this.bossInfo.Id
                ,maxmin: true
                ,area: ['80%', '80%']
                ,btn: [ '关闭']
                ,yes: function(index, layero){
                    layer.close(index);
                }
            });
        },ticketsFunc(){
            var _this=this;
            _this.ticketsFuncreadnum();
            layer.open({
                type: 2
                ,title: '工单列表'
                ,content: '/admin/tickets/list?boss_id='+_this.bossInfo.Id
                ,maxmin: true
                ,area: ['80%', '80%']
                ,btn: [ '关闭']
                ,yes: function(index, layero){
                    layer.close(index);
                }
            });
        },BossTjFunc(){
            var _this=this;
            layer.open({
                type: 2
                ,title: _this.bossInfo.BossName+'：近30日访客统计'
                ,content: '/admin/bosstj/?boss_id='+_this.bossInfo.Id
                ,maxmin: true
                ,area: ['98%', '90%']
                ,btn: [ '关闭']
                ,yes: function(index, layero){
                    layer.close(index);
                }
            });
        },
        ipblacklistFunc(){
            var _this=this;
            layer.open({
                type: 2
                ,title: '商家Ip黑名单(拉黑后，该ip将无法访问访客页面)'
                ,content: '/admin/blackip/?boss_id='+_this.bossInfo.Id
                ,maxmin: true
                ,area: ['80%', '80%']
                ,btn: [ '关闭']
                ,yes: function(index, layero){
                    layer.close(index);
                }
            });
        },
        blacklistVisitor(_state){
            var _this=this;
            DianJinAjax("1",{dataCode: false,resCode: true},{
                url:'/admin/api/visitorlh',
                type:'POST',
                data:{visitor_id: _this.visitorinfo.VisiterId,state:_state},
                success:function(result){

                    if(result.code==200){
                        _this.visitorinfo.State=_state;
                        layer.close(index); //关闭弹层
                    }
                    layer.msg(result.msg);
                }});
        },
        adminService(){
            layer.open({
                type: 2
                ,title: '管理我的客服小二'
                ,content: '/admin/visitor/service'
                ,maxmin: true
                ,area: ['90%', '90%']
                ,btn: [ '关闭']

            });
        },
        adminLookWarn(warnmsg,index){
            var _this=this;
            _this.warnmsgList[index].Isshow=0;
            var isshownum=_this.warnmsgList.length;
            for (inde in _this.warnmsgList){
                if(_this.warnmsgList[inde].Isshow==0){
                    isshownum--;
                }
            }
            if(isshownum==0){
                _this.warnmsgList=[];
            }
            var url="/admin/warn/info?id="+warnmsg.Id;
            if(warnmsg.Type=="tickets"){
                url="/admin/tickets/from/update?no="+warnmsg.Linkno;
            }

            layer.open({
                type: 2
                ,title: '详情'
                ,content: url
                ,maxmin: true
                ,area: ['90%', '90%']
                ,btn: [ '新窗口打开','关闭']
                ,yes:function () {
                    window.open(url);
                }

            });
        }
    },

})


