
function editServiceId(no){
    layer.open({
        type: 2
        ,title: '分配受理人员 ：'+no
        ,content: '/admin/tickets/from/fpservice?no='+no
        ,maxmin: true
        ,area: layer_open_area(['300px', '400px'])
        ,btn: [ '确定','取消']
        ,yes: function (index,layero) {
            var iframeWindow = window['layui-layer-iframe'+ index]
                ,submitID = 'LAY-user-role-submit'
                ,submit = layero.find('iframe').contents().find('#'+ submitID);

            //监听提交
            iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                var field = data.field; //获取提交的字段
                if (!field.service_id){
                    layer.msg("请选择受理人员")
                    return;
                }
                //提交 Ajax 成功后，静态更新表格中的数据
                DianJinAjax("1",{dataCode: false,resCode: false},{
                    url:'/admin/tickets/from/fpservice',
                    type:'POST',
                    data:{"no":no,"service_id":field.service_id-0},
                    success:function(result){

                        if(result.code==200){
                            table.reload(table_elem_id); //数据刷新
                            layer.close(index); //关闭弹层
                        }else{
                            layer.alert(result.msg);
                        }

                    }});

            });

            submit.trigger('click');

        }
    });
}

var ismy=getQueryString("ismy")-0;


var $,table,form,table_elem_id;
layui.use(['table','form'], function(){
     $ = layui.$
     ,table = layui.table
     ,form = layui.form
     ,table_elem_id='layui-data-table';

    $("#ismy"+ismy).addClass("layui-this");

    table.render({
      elem: '#'+table_elem_id
       ,method:'post'
      ,url: '?ismy='+ismy
      ,parseData: function(res){ //res 即为原始返回的数据
            res =DianJinAjaxSuccess(res);
        return {
          "code": res.code==200?0:res.code, //解析接口状态
          "msg": res.message, //解析提示文本
          "count": res.data.page.Total, //解析数据长度
          "data": res.data.data //解析数据列表
        };
      }
       ,defaultToolbar: ['filter','exports','print']
      ,toolbar:'#layui-data-table-toolbar'
      ,height: 'full-100'
      ,cellMinWidth: 80
      
      ,page: true
      ,limit: 15
      ,cols: [[
        {title: '序号',templet: '#indexTpl',type:'numbers', fixed: 'left' }
        ,{type:'checkbox', fixed: 'left' }
            ,{field:'BossName',title: '商家',width: 100}
            ,{field:'TicketNo',title: '工单号',width: 100}
            ,{field:'Typec',title: '工单类型',width: 100}

        ,{field:'Title',title: '工单标题',minWidth: 220}
            ,{field:'UserNick',title: '提交者',width: 130}
            ,{field:'ServiceNick',title: '受理人员',width: 136,templet: function(res){

                    if (res.ServiceId==0){
                        return '<button class="layui-btn  layui-btn-xs" onclick="editServiceId(\''+res.TicketNo+'\')">点击分配</button>';
                    }
                    return '<span>'+res.ServiceNick+'</span> <button class="layui-btn  layui-btn-primary layui-btn-xs" onclick="editServiceId(\''+res.TicketNo+'\')">转</button>';
                }}


        /*,{field:'AdminId',title: '管理员id'}*/
        /*,{field:'latitude',title: '纬度'}*/
            /*,{field:'Grade',title: '工单等级',width: 108,sort:true,templet: function(res){
                    var sta = {
                        "0":'<button class="layui-btn layui-btn-primary layui-btn-xs">0一般工单</button>',
                        "1":'<button class="layui-btn layui-btn-warm layui-btn-xs">1紧急工单</button>',
                        "2":'<button class="layui-btn layui-btn-normal layui-badge layui-btn-xs">2非常紧急工单</button>',
                        "3":'<button class="layui-btn layui-btn-normal layui-btn-xs" style="background: green">3优先工单</button>'
                    };

                    return sta[res.Grade]? sta[res.Grade]: sta["0"];
                }}*/
        ,{field:'Status',title: '工单状态',width: 108,sort:true,templet: function(res){
                    var sta = {
                        "-1":'<button class="layui-btn layui-btn-warm layui-btn-xs">待处理</button>',
                        "0":'<button class="layui-btn layui-btn-warm layui-btn-xs">待处理</button>',
                        "1":'<button class="layui-btn  layui-btn-primary layui-btn-xs">已处理</button>',
                        "2":'<button class="layui-btn  layui-btn-xs" style="background: #c0c0c0">已关闭</button>'
                    };

                    return sta[res.Status]? sta[res.Status]: sta["-1"];
                }}
            ,{field:'Addtime',title: '提交时间',width: 180}
        ,{fixed: 'right', width: 70, align:'center',title:'操作' ,templet: function(res){
        var sta = {
            "-1":'<button class="layui-btn  layui-btn-xs" lay-event="edit">处理</button>',
            "0":'<button class="layui-btn  layui-btn-xs" lay-event="edit">处理</button>',
            "1":'<button class="layui-btn  layui-btn-primary layui-btn-xs" lay-event="edit">查看</button>',
            "2":'<button class="layui-btn  layui-btn-primary layui-btn-xs" lay-event="edit">查看</button>',
        };

        return sta[res.Status]? sta[res.Status]: sta["-1"];
    }}

      ]]
    });


    var submitFuc=function(index, layero){
        var iframeWindow = window['layui-layer-iframe'+ index]
            ,submitID = 'LAY-user-role-submit'
            ,submit = layero.find('iframe').contents().find('#'+ submitID)
            ,inputtypeints = layero.find('iframe').contents().find('#inputtypeints');
            if($(inputtypeints)[0])inputtypeints=$(inputtypeints)[0].value;
        //监听提交
        iframeWindow.layui.form.on('submit('+submitID+')', function(data){
            var field = data.field; //获取提交的字段
            var fieldint = (inputtypeints+"").split(",");
           // console.log(field)
            //console.log(fieldint)
            for (d in field){
                if (fieldint.indexOf(d)!=-1){
                    field[d]=field[d]-0;
                }
            }
            //console.log(field)
            //提交 Ajax 成功后，静态更新表格中的数据
            DianJinAjax("1",{dataCode: true,resCode: false},{
                url:'/admin/boss/save',
                type:'POST',
                data:field,
                success:function(result){
                    layer.msg(result.msg)
                    if(result.code==200){
                        table.reload(table_elem_id); //数据刷新
                        layer.close(index); //关闭弹层
                    }

                }});

        });

        submit.trigger('click');
    }

    var add=function(){
        layer.open({
            type: 2
            ,title: '提交新工单'
            ,content: '/tickets/from?boss_id='+getQueryString("boss_id")
            ,area: layer_open_area(['100%', '100%'])
            ,maxmin: true
            ,btn: [ '取消']
            ,yes: function (index) {
                layer.close(index); //关闭弹层
                table.reload(table_elem_id);
            }
        });
    };
    
    var edit=function(id){
     layer.open({
                type: 2
                ,title: '处理工单号：'+id
                ,content: '/admin/tickets/from/update?no='+id
                ,maxmin: true
                ,area: layer_open_area(['100%', '100%'])
                ,btn: [ '取消']
                ,yes: function (index) {
             layer.close(index); //关闭弹层
             table.reload(table_elem_id);
         }
            });
    };


 var deleFuc=function(ids){
            layer.confirm('确定删除吗？', function(index) {
                //执行 Ajax 后重载
                DianJinAjax("1",{dataCode: false,resCode: false},{
                    url:'/admin/boss/del',
                    type:'get',
                    data:{ids:ids},
                    success:function(result){
                        layer.close(index);
                        layer.msg(result.msg)
                        if(result.code==200){
                            table.reload(table_elem_id);
                        }

                    }});

            });
        }
 var infoFuc=function(id){
     layer.open({
                type: 2
                ,title: '详情'
                ,content: '/admin/boss/info?id='+id
                ,maxmin: true
                ,area: layer_open_area(['100%', '100%'])
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    layer.close(index); //关闭弹层
                }
            });
    };
        //监听行工具事件
    table.on('tool('+table_elem_id+')', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            ,layEvent = obj.event; //获得 lay-event 对应的值

        if(layEvent === 'info'){
           infoFuc(data.Id);
        } else if(layEvent === 'del'){
            deleFuc(data.Id);
        }else if(layEvent === 'edit'){
            edit(data.TicketNo);
        }
    });






    //监听排序
     table.on('sort('+table_elem_id+')', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        console.log(obj.field); //当前排序的字段名
        console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
        console.log(this) //当前排序的 th 对象*/

        return false;
    });




     //头工具栏事件
  table.on('toolbar('+table_elem_id+')', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'delAll':
         var checkStatus = table.checkStatus(table_elem_id)
        ,checkData = checkStatus.data; //得到选中的数据

        if(checkData.length === 0){
          return layer.msg('请选择数据');
        }
        var ids=[];
        for(var i in checkData){
            ids.push(checkData[i].Id);
        }
        deleFuc(ids.join(','));
      break;
      case 'add':
        add();
      break;
      //自定义头工具栏右侧图标 - 提示
      case 'ShowSearch':
      var display=$("#searchBox").css('display');
       $("#searchBox").css('display',display=='block'?'none':'block');
      break;
    };
  });


    form.on('submit(demo-table-search)', function(data){
        var field = data.field; // 获得表单字段
        field.ismy=ismy;
        // 执行搜索重载
        table.reload(table_elem_id, {
            page: {
                curr: 1 // 重新从第 1 页开始
            },
            where: field // 搜索的字段
        });
       // layer.msg('搜索成功<br>此处为静态模拟数据，实际使用时换成真实接口即可');
        return false; // 阻止默认 form 跳转
    });


})