
var $;
layui.use(['form'], function(){
    var $ = layui.$
        ,form = layui.form
        //表id
        ,tableelemId = 'LAY-user-back-manage'
        ,table = layui.table;



    table.render({
        elem: '#'+tableelemId
        ,url:'?status='+status
        ,parseData: function(res){ //res 即为原始返回的数据
            return {
                "code": res.code==200?0:res.code, //解析接口状态
                "msg": res.message, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data //解析数据列表
            };
        }
        ,toolbar: '#toolbarDemo'
        ,title: '用户订单'
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,totalRow: true
        ,limit:10
        ,limits:[10,20,50,100]
        ,cols: [[
            {type: 'checkbox', fixed: 'left'},

            // {title: '序号',templet: '#indexTpl',type:'numbers', fixed: 'left', totalRowText: '合计'}
            {field:'id', title:'ID', width:80}
            ,{field:'stitle', title:'部门', Width:120}
            ,{field:'username', title:'用户名', Width:130}
            ,{field:'title', title:'用户组', Width:100}
            ,{field:'status', title:'状态', width:80, templet: function(res){
                    var statusk=['<button class="layui-btn layui-btn-primary layui-btn-xs">封锁</button>','<button class="layui-btn layui-btn-xs">正常</button>'];
                    return statusk[res.status];
                }}

            ,{field:'realname', title:'姓名', minWidth:80}
            //,{field:'nick', title:'昵称', minWidth:80}
            /*,{field:'touxiang', title:'头像', width:80, templet: function(res){
                    return '<img src="'+res.touxiang+'" onclick="lookMaxImg(\''+res.touxiang+'\')"   height="100%" alt="">';
                }}*/
            ,{field:'mobile', title:'电话', minWidth:80}
            ,{field:'email', title:'邮箱', minWidth:80}
            //,{field:'remark', title:'备注', minWidth:100}


            ,{field:'create_time', title:'创建时间', width:120,sort:true}
            //,{field:'login_time', title:'登陆时间', width:120}
            //,{field:'login_ip', title:'登陆IP', width:120}

            ,{fixed: 'right', title:'操作', toolbar: '#table-useradmin-admin', width:350}
        ]]
        ,page: true
    });


    var deleac=function(ids){
        layer.confirm('确定删除吗？', function(index) {

            //执行 Ajax 后重载
            $.ajax({
                url:'./dele',
                type:'POST',
                data:{ids:ids},
                success:function(result){
                    console.log(result)
                    layer.close(index);
                    layer.msg(result.message)
                    if(result.code==200){
                        table.reload(tableelemId);
                    }

                }});

        });
    }


    //监听行工具事件
    table.on('tool('+tableelemId+')', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            ,layEvent = obj.event; //获得 lay-event 对应的值

        if(layEvent === 'detail'){
            layer.msg('查看操作');
        } else if(layEvent === 'del'){
            deleac(data.id)
        } else if(layEvent === 'repaw'){
            layer.confirm('确定要重置密码吗？', function(index) {

                //执行 Ajax 后重载
                $.ajax({
                    url:'./repaw',
                    type:'POST',
                    data:{id:data.id},
                    success:function(result){
                        layer.close(index);
                        layer.msg(result.message)


                    }});

            });
        } else if(layEvent === 'edit'){
            layer.open({
                type: 2
                ,title: '修改管理员'
                ,content: './edit?id='+data.id
                ,area: ['420px', '620px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                        var field = data.field; //获取提交的字段

                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url:'./save',
                            type:'POST',
                            data:field,
                            success:function(result){

                                layer.msg(result.message)
                                if(result.code==200){
                                    table.reload(tableelemId); //数据刷新
                                    layer.close(index); //关闭弹层
                                }

                            }});

                    });

                    submit.trigger('click');
                }
            });

        }else if(layEvent === 'rule'){
            layer.open({
                type: 2
                ,title: '栏目授权'
                ,content: './rule.html?id='+data.id
                ,area: ['420px', '620px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                        var field = data.field; //获取提交的字段
                        var postdata={};
                        var rules=[];
                        for(var field1 in field){
                            if(field1.indexOf('layuiTreeCheck')!=-1){
                                rules.push(field[field1]);
                            }else{
                                postdata[field1]=field[field1];
                            }

                        }
                        postdata.rules=rules.join(',')
                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url:'./save',
                            type:'POST',
                            data:postdata,
                            success:function(result){

                                layer.msg(result.message)
                                if(result.code==200){
                                    table.reload(tableelemId); //数据刷新
                                    layer.close(index); //关闭弹层
                                }

                            }});

                    });

                    submit.trigger('click');
                }
            });

        }
    });

    //监听排序
    table.on('sort('+tableelemId+')', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        console.log(obj.field); //当前排序的字段名
        console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
        console.log(this) //当前排序的 th 对象*/

        return false;
    });

    //监听搜索
    form.on('submit(LAY-user-back-search)', function(data){
        var field = data.field;

        //执行重载
        table.reload(tableelemId, {
            where: field
        });
    });

    //事件
    var active = {
        batchdel: function(){
            var checkStatus = table.checkStatus(tableelemId)
                ,checkData = checkStatus.data; //得到选中的数据
            if(checkData.length === 0){
                return layer.msg('请选择数据');
            }
            var ids=[];
            for(var iks=0;iks<checkData.length;iks++){
                ids.push(checkData[iks]['id']);
            }
            ids=ids.join(',');
            deleac(ids);
        }
        ,add: function(){
            layer.open({
                type: 2
                ,title: '添加管理员'
                ,content: './add'
                ,area: ['420px', '620px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+submitID+')', function(data){
                        var field = data.field; //获取提交的字段

                        //提交 Ajax 成功后，静态更新表格中的数据
                        $.ajax({
                            url:'./save',
                            type:'POST',
                            data:field,
                            success:function(result){

                                layer.msg(result.message)
                                if(result.code==200){
                                    table.reload(tableelemId); //数据刷新
                                    layer.close(index); //关闭弹层
                                }

                            }});

                    });

                    submit.trigger('click');
                }
            });
        }

    }

    $('.layui-btn.layuiadmin-btn-admin').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
});


