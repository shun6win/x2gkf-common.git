function x2gVipInfo(){
    layer.open({
        type: 2
        ,title: '版本介绍'
        ,content:'/x2ginfo'
        ,maxmin: true
        ,area: ['80%', '80%']
        ,btn: ['取消']

    });
}


function UpdateX2gkf(ismsg=false) {
    DianJinAjax("1",{dataCode: false,resCode: false},{
        url: '/admin/update/check',
        type:'get',
        data:{},
        success:function(result){
            if(result.code==204){
                // 被设置不主动跟新
                $("#verupdate").hide();
            }
            $("#ver").html("V."+result.data.ver);
            $("#ver"+result.data.vertype).show();
            if(result.data.vertype==9){
                $("#ver20").hide();
            }
            if(result.data.vernum-result.data.ver>0){
                $("#verts").show();
            }
            if(result.code==200){
                if(result.data.vernum-getCookie("UpdateX2gkf")!=0 || ismsg){
                    UpdateX2gkfBox(result.data);
                }
            }else if(ismsg){
                layer.alert(result.msg?result.msg:"当前为最新版本");
                return;
            }

        }});
}
var updatecData;
function UpdateX2gkfBox(data) {
    updatecData=data;
    layer.open({
        type: 1
        ,title: false
            ,closeBtn: 2,
            shadeClose: false,
            skin: 'layui-layer-dialog new-layer-update active'

        ,content: '<img class="update_bg" src="/public/style_js_admin/images/update_1.png" alt=""><div id="updatebox" style="margin-top: 120px" >' +
            '<h2 style="text-align: center">发现新的二鸽版本，是否立即更新？</h2><h3 style="text-align: center">最新版本：'+data.vernum+'</h3>' +
            /*'<blockquote class="layui-elem-quote layui-text">\n' +*/
           /* data.title +*/
            '</blockquote>'+
            '  <h3 style="padding-bottom: 10px"></h3>\n' +
            '<blockquote class="layui-elem-quote layui-text">'+data.content.replace(/\n/g,"</br>")+'</blockquote>'+
            '<div id="updatelogbox" style="display: none"> <h3>更新进度日志</h3><pre id="updatelog" class="layui-code" style="background: #000;color: wheat;" lay-skin="notepad"></pre> </div>'+
            '</div>'
        ,maxmin: false
        ,area: ['420px', '620px']
        ,btn: ['立即更新','忽略此版本']
        ,btn1:  okupdate
        ,btn2:  updateHl
    });
}

function updateHl() {
    setCookie("UpdateX2gkf",updatecData.vernum);
}

var isupdata=false;
function okupdate() {
    if (isupdata){
        layer.alert("正在执行更新，请稍后……，如长时间没有反应请刷新重试");
        return;
    }
    layer.prompt({title: '您正在进行： 【客服系统升级，期间可能会重启程序】 请输入登入密码，并确定进行确定', formType: 1}, function(text, index){

        //执行 Ajax 后重载
        DianJinAjax(1,{dataCode: true,resCode: false}, {
            url: '/admin/update/start',
            data:{pass:text},
            type: "POST",
            success: function (res) {
                if(res.code==200){
                    isupdata=true;
                    layer.close(index);
                    checkupdatelog();

                }else{
                    layer.alert(res.msg);
                }

            }
        });


    });

}

var updatelogold="";
var updatelognum=0;
function checkupdatelog() {
    updatelognum++;
    DianJinAjax("1",{dataCode: false,resCode: false},{
        url: '/admin/update/showlog',
        type:'get',
        data:{},
        success:function(result){
            if(result.code==200){
                $("#updatelogbox").show();
                if (updatelogold==result.data){
                    var ingtext='<br><span style="color: white">进行中……(请耐心等待)</span><br>';
                    if(updatelognum%2==0){
                        ingtext='<br><span style="color: #00BF6D">进行中…………</span><br>';
                    }
                    $("#updatelog").html(result.data.replace(/\n/g,"</br>")+ingtext+updatelognum+"s");
                }else{
                    $("#updatelog").html(result.data.replace(/\n/g,"</br>"));
                }
                updatelogold=result.data;


                if (result.data.indexOf("退出更新")==-1 && result.data.indexOf("更新完成")==-1){
                    setTimeout(checkupdatelog,1000);
                }else{
                    isupdata=false;
                    if(result.data.indexOf("更新完成")!=-1){
                        isupdata=true;
                        layer.alert("更新完成,即将为您刷新,(ps:如需同步更新app版本 请前往-> APP版本更新信息 底部中同步更新)");
                        setTimeout(function () {
                            window.location.reload();
                        },8000);
                    }else if(result.data.indexOf("退出更新")!=-1){
                        layer.alert("更新失败，请查看更新进度日志");
                    }
                }
                //滑动到更新日志底部
                $(".layui-layer-content")[0].scrollTo(0,document.getElementById("updatebox").scrollHeight);


            }

        }});
}



function rebootX2gkf() {

    layer.prompt({title: '您正在进行： 【重启二鸽服务】 请输入登入密码，并确定进行确定', formType: 1}, function(text, index){

        //执行 Ajax 后重载
        DianJinAjax(1,{dataCode: true,resCode: false}, {
            url: '/admin/reboot',
            data:{pass:text},
            type: "POST",
            success: function (res) {
                if(res.code==200){
                    layer.close(index);
                }else{
                    layer.alert(res.msg);
                }

            },error:function(xhr,status,error){
                layer.alert("已为您重启，即将刷新");
                setTimeout(function () {
                    window.location.reload();
                },5000);
            }
        });


    });

}





