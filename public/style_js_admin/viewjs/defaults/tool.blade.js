var $,form;

layui_use()
function form_render(time,fun) {
    time = time || 120;
    setTimeout(function (){
        form.render();
        if(fun){
            fun();
        }
    },time)
}
function layui_use(){
    setTimeout(function () {

            layui.use(['form'], function() {
                $ = layui.$
                    , form = layui.form
                submitID = 'layuiadmin-form-role';
                form.render();

                form.on('select(dbtable)', function (data) {
                    vuell.select_table = vuell.tables[data.value];
                    //vuell.list_table = vuell.select_table;
                    vuell.list_table_edit(vuell.select_table);
                    vuell.controller =firstToUpper1(data.value.replace(vuell.table_pf,"").replace(/_/g,""))+"Controller";
                    //vuell.list_table = [];
                    form_render();
                });

                form.on('select(retable)', function (data) {
                    console.log(data)
                    var keydata=data.value.split('-');
                    vuell.re_tables[keydata[1]] = vuell.tables[keydata[0]];
                    vuell.$set(vuell.re_tables,vuell.re_tables)
                    //console.log(vuell.tables[keydata[0]])
                    vuell.list_table_edit(vuell.tables[keydata[0]],'a'+keydata[1]+'.');
                    form_render(200,function (){
                        var data1 = form.val(submitID);
                        console.log(data1)
                    });
                });

            });
        }
        ,200);
}

var vuell= new Vue({
    el: "#vueapp",
    data:{
        routes:{
            "add":1,
            "dele":1,
            "edit":1,
            "list":1,
        },
        check_types:{
            'required':'必填项',
            'required|phone':'手机号',
            'required|email':'邮箱',
            'required|url':'网址',
            'required|number':'数字',
            'required|date':'日期',
            'required|identity':'身份证',
        },
        input_types:{
            'input':'单行输入框',
            'textarea':'多行输入框',
            'select':'下拉选择框',
            'radio':'单选框',
            'checkbox':'多选框',
            'uploadimage':'图片上传',
            'uploadfile':'文件上传',
            'layedit':'layui富文本编辑器',
        },
        controller:'',
        controllername:'',
        table_pf:'',//表前缀
        tables:[],
        select_table:{},
        list_table:[],
        re_tables:[],//关联表
        re_tables_table:[],//已关联表
        re_tables_fi:[],//已关联字段
    },
    mounted () {
        this.get_tables()
    },
    methods: {
        submit(){
            var formdata = form.val(submitID);
            if(formdata.table==''){
                layer.msg("请选择数据库");return;
            }
            if(formdata.controller==''){
                layer.msg("控制器不能为空");return;
            }
            var _this=this;
            var pushdata ={
                'table':formdata.table.replace(vuell.table_pf,""),
                'controller':formdata.controller,
                'controllername':formdata.controllername,
                'add':{
                    'status':_this.routes.add,
                    'list':[]
                },
                'edit':{
                    'status':_this.routes.edit,
                    'list':[]
                },
                'dele':{
                    'status':_this.routes.dele,
                    'list':[]
                },'lists':{
                    'status':_this.routes.list,
                    'list':[]
                },'join':[]
                ,'buttons_top':[]
                ,'buttons_rigth':[]
                ,'buttons_row':[]
            };


             if(pushdata.add.status || pushdata.edit.status ){
                 var i=0;
                 for (var it in _this.select_table){
                     var add_fied = 'add_fied[' + i + ']';
                     var add_type = 'add_type[' + i + ']';
                     var add_title = 'add_title[' + i + ']';
                     var add_checktype = 'add_checktype[' + i + ']';
            if(formdata[add_fied]){
                pushdata.add.list.push({
                    "fied":formdata[add_fied],
                    'type':formdata[add_type],
                    'title':formdata[add_title],
                    'checktype':formdata[add_checktype],
                });
            }
                     i++;
                 }
                 pushdata.edit.list = pushdata.add.list;
             }

            if(pushdata.lists.status){
                var i=0;
                for (var it in _this.list_table){
                    var list_fied = 'list_fied[' + i + ']';
                    var list_total = 'list_total[' + i + ']';
                    var list_title = 'list_title[' + i + ']';
                    var list_so = 'list_so[' + i + ']';
                    var list_order = 'list_order[' + i + ']';
                    if(formdata[list_fied]){
                        pushdata.lists.list.push({
                            "fied":formdata[list_fied]??0,
                            'total':formdata[list_total]??0,
                            'title':formdata[list_title]??0,
                            'so':formdata[list_so]??0,
                            'order':formdata[list_order]??0,
                        });
                    }
                    i++;
                }
                var i=0;
                for (var it in _this.re_tables){
                    var re_tables = 're_tables[' + i + ']';
                    var re_tables_fi_old = 're_tables_fi_old[' + i + ']';
                    var re_tables_fi = 're_tables_fi[' + i + ']';
                    var re_tables_type = 're_tables_type[' + i + ']';
                    if(formdata[re_tables] && formdata[re_tables_fi_old] && formdata[re_tables_fi]){
                        pushdata.join.push({
                            "fied":formdata[re_tables_fi]??0,
                            'table':formdata[re_tables].split('-')[0]+' as a'+i,
                            'fied_old':formdata[re_tables_fi_old],
                            'join':formdata[re_tables_type]??'left',
                        });
                    }
                    i++;
                }

                for (var i=0;i< 10;i++){
                    var buttons_top = 'buttons_top[' + i + ']';
                    if(formdata[buttons_top]){
                        pushdata.buttons_top.push(formdata[buttons_top]);
                    }
                    var buttons_row = 'buttons_row[' + i + ']';
                    if(formdata[buttons_row]){
                        pushdata.buttons_row.push(formdata[buttons_row]);
                    }
                    var buttons_rigth = 'buttons_rigth[' + i + ']';
                    if(formdata[buttons_rigth]){
                        pushdata.buttons_rigth.push(formdata[buttons_rigth]);
                    }
                }
            }

            console.log(formdata)
            console.log(pushdata)



            $.ajax({
                url:'/tool/make?',
                type:'POST',
                data:pushdata,
                success:function(result){
                    console.log(result)
                }});
        },
        remove_re_tables(index){
            this.re_tables.splice(index,1);
        },
        add_re_tables(){
            this.re_tables.push([]);
            form_render();
        },
        list_table_edit(tabledata,fx){
            fx = fx || '';
            // this.list_table = [];
            for (var i in tabledata ){
                tabledata[i].order=0;
                tabledata[i].total=0;
                tabledata[i].so=0;
                tabledata[i].check=fx?0:1;
                tabledata[i].fied=fx+tabledata[i].fied;
                this.list_table.push(tabledata[i]);
            }
            console.log(tabledata)
        },
        deleMake(){
            var _this = this;
            if(_this.controller==''){
                layer.alert("请先选择库或者直接填写控制器名称！");
                return;
            }
            layer.confirm('本次删除会删除 '+_this.controller+' 之前生成的所有文件，确定吗？', function(index) {

                //执行 Ajax 后重载
                $.ajax({
                    url:'/tool/dele',
                    type:'POST',
                    data:{controller:_this.controller},
                    success:function(result){
                        layer.msg(result.message)
                        if(result.code==200){
                            _this.get_data();
                        }

                    }});

            });
        },
        get_tables(){
            var _this=this;
            //提交 Ajax 成功后，静态更新表格中的数据
            $.ajax({
                url:'?',
                type:'GET',
                success:function(result){
                    if(result.code==200){
                        _this.tables = result.data.tables;
                        _this.table_pf = result.data.table_prefix;

                        layui_use()

                    }

                }});
        }
    },
});


