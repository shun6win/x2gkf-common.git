
/*!
 * layui 官网
 */

layui.define(['code', 'element', 'table', 'util', 'carousel', 'laytpl'], function(exports){
    var $ = layui.jquery
        ,element = layui.element
        ,layer = layui.layer
        ,form = layui.form
        ,util = layui.util
        ,carousel = layui.carousel
        ,laytpl = layui.laytpl
        ,device = layui.device()

        ,$win = $(window), $body = $('body');

    //ban iframe
    ;!function(){self!==parent&&(location.href="//www.baidu.com/")}();


    //阻止 IE7 以下访问
    if(device.ie && device.ie < 8){
        layer.alert('Layui 最低支持 IE8，而您当前使用的是古老的 IE'+ device.ie + '，体验将会不佳！');
    }

    var home = {
        //获取高级浏览器
        getBrowser: function(){
            var ua = navigator.userAgent.toLocaleLowerCase()
                ,mimeType = function(option, value) {
                var mimeTypes = navigator.mimeTypes;
                for (var key in mimeTypes) {
                    if (mimeTypes[key][option] && mimeTypes[key][option].indexOf(value) !== -1) {
                        return true;
                    }
                }
                return;
            };
            if(ua.match(/chrome/)){
                if(mimeType('type', '360') || mimeType('type', 'sogou')) return;
                if(ua.match(/edg\//)) return 'edge';
                return 'chrome'
            } else if(ua.match(/firefox/)){
                return 'firefox';
            }

            return;
        }
    };

    var elemHome = $('#LAY_home');
    var local = layui.data('layui');

    //初始弹窗
    layer.ready(function(){
return;
        //升级提示
        if(local.version && local.version !== layui.v){
            layer.open({
                type: 1
                ,title: '更新提示' //不显示标题栏
                ,closeBtn: false
                ,area: '300px;'
                ,shade: false
                ,offset: 'b'
                ,id: 'LAY_updateNotice' //设定一个id，防止重复弹出
                ,btn: ['更新日志', '我知道了']
                ,btnAlign: 'c'
                ,moveType: 1 //拖拽模式，0或者1
                ,content: ['<div class="layui-text">'
                    ,'layui 已发布新版本：：<strong style="padding-right: 10px; color: #fff;">v'+ layui.v + '</strong>'
                    ,'</div>'].join('')
                ,skin: 'layui-layer-notice'
                ,yes: function(index){
                    layer.close(index);
                    setTimeout(function(){
                        location.href = '/doc/base/changelog.html';
                    }, 500);
                }
                ,end: function(){
                    layui.data('layui', {
                        key: 'version'
                        ,value: layui.v
                    });
                }
            });
        }
        layui.data('layui', {
            key: 'version'
            ,value: layui.v
        });
    });




    //点击事件
    var events = {
        //联系方式
        contactInfo: function(){
            layer.alert('<div class="layui-text">如有合作意向，可联系：<br>邮箱：</div>', {
                title:'联系'
                ,btn: false
                ,shadeClose: true
            });
        }
        //公众号
        ,weixinmp: function(){
            layer.photos({
                photos: {
                    data: [{
                        alt: 'layui 公众号'
                        ,"src": "" //原图地址
                        ,"thumb": "" //缩略图地址
                    }]
                }
            })
        }
    };

    $body.on('click', '*[site-event]', function(){
        var othis = $(this)
            ,attrEvent = othis.attr('site-event');
        events[attrEvent] && events[attrEvent].call(this, othis);
    });


    //切换版本
    form.on('select(tabVersion)', function(data){
        var value = data.value;
        location.href = value === 'new' ? '/' : ('/' + value + '/doc/');
    });


    //首页 banner
    setTimeout(function(){
        $('.site-zfj').addClass('site-zfj-anim');
        setTimeout(function(){
            $('.site-desc').addClass('site-desc-anim')
        }, 5000)
    }, 100);


    //数字前置补零
    var digit = function(num, length, end){
        var str = '';
        num = String(num);
        length = length || 2;
        for(var i = num.length; i < length; i++){
            str += '0';
        }
        return num < Math.pow(10, length) ? str + (num|0) : num;
    };


    //下载倒计时
    var setCountdown = $('#setCountdown');
    if($('#setCountdown')[0]){
        $.get('/api/getTime', function(res){
            util.countdown(new Date(2017,7,21,8,30,0), new Date(res.time), function(date, serverTime, timer){
                var str = digit(date[1]) + ':' + digit(date[2]) + ':' + digit(date[3]);
                setCountdown.children('span').html(str);
            });
        },'jsonp');
    }


    //Adsense
    ;!function(){
        var len = $('.adsbygoogle').length;
        try {
            for(var i = 0; i < len; i++){
                (adsbygoogle = window.adsbygoogle || []).push({});
            }
        } catch (e){
            console.error(e)
        }
    }();



    //展示当前版本
    $('.site-showv').html(layui.v);

    //获取Github数据
    var getStars = $('#getStars');
    if(getStars[0]){
        var res = {"stargazers_count":'8.1k'}
        getStars.html(res.stargazers_count);
    }

    //首页操作
    (function(){
        var elemDowns = $('.site-showdowns');
        //获取下载数
        if(elemDowns[0]){
            var res = {"number":2356878,"title":"layui下载量"};
            elemDowns.html(res.number);
        }

        //记录下载
        $('.site-down').on('click',function(e){
            var othis = $(this)
                ,local = layui.data('layui')
                ,setHandle = function(){

            };
            if(!local.disclaimer){
                e.preventDefault();

                layer.confirm([
                    '<div class="layui-text" style="padding: 10px 0;">'
                    ,'请先阅读《<a href="/www.layui.com/about/disclaimer.html" target="_blank">layui 开源界面框架免责声明</a>》'
                    ,'，再进行下载</div>'
                ].join(''), {
                    title: '下载提示'
                    ,btn: ['已阅读', '取消']
                    ,maxWidth: 750
                }, function(index){
                    layui.data('layui', {
                        key: 'disclaimer'
                        ,value: new Date().getTime()
                    });
                    layer.close(index);

                    othis[0].click();
                    setHandle();
                });
            } else {
                setHandle();
            }
        });
    })();


    //固定Bar
    util.fixbar({
        showHeight: 60
        ,css: function(){
            if(global.pageType === 'demo'){
                return {bottom: 75}
            }
        }()
    });

    //窗口scroll
    ;!function(){
        var main = $('.site-menu'), scroll = function(){
            var stop = $(window).scrollTop();

            if($(window).width() <= 992) return;
            var bottom = 0;

            if(stop > 60){ //211
                if(!main.hasClass('site-fix')){
                    main.addClass('site-fix').css({
                        width: main.parent().width()
                    });
                }
            }else {
                if(main.hasClass('site-fix')){
                    main.removeClass('site-fix').css({
                        width: 'auto'
                    });
                }
            }
            stop = null;
        };
        scroll();
        $(window).on('scroll', scroll);
    }();

    //示例页面滚动
    $(window).on('scroll', function(){
        /*
        var elemDate = $('.layui-laydate,.layui-colorpicker-main')
        if(elemDate[0]){
          elemDate.each(function(){
            var othis = $(this);
            if(!othis.hasClass('layui-laydate-static')){
              othis.remove();
            }
          });
          $('input').blur();
        }
        */

        var elemTips = $('.layui-table-tips');
        if(elemTips[0]) elemTips.remove();

        if($('.layui-layer')[0]){
            layer.closeAll('tips');
        }
    });

    //代码修饰
    layui.code({
        elem: 'pre'
    });

    //目录
    var siteDir = $('.site-dir');
    if(siteDir[0] && $(window).width() > 750){
        layer.ready(function(){
            layer.open({
                type: 1
                ,content: siteDir
                ,skin: 'layui-layer-dir'
                ,area: 'auto'
                ,maxHeight: $(window).height() - 300
                ,title: '目录'
                ,closeBtn: false
                ,offset: 'r'
                ,shade: false
                ,success: function(layero, index){
                    layer.style(index, {
                        marginLeft: -15
                    });
                }
            });
        });
        siteDir.find('li').on('click', function(){
            var othis = $(this);
            othis.find('a').addClass('layui-this');
            othis.siblings().find('a').removeClass('layui-this');
        });
    }

    //在textarea焦点处插入字符
    var focusInsert = function(str){
        var start = this.selectionStart
            ,end = this.selectionEnd
            ,offset = start + str.length

        this.value = this.value.substring(0, start) + str + this.value.substring(end);
        this.setSelectionRange(offset, offset);
    };

    //演示页面
    $('body').on('keydown', '#LAY_editor, .site-demo-text', function(e){
        var key = e.keyCode;
        if(key === 9 && window.getSelection){
            e.preventDefault();
            focusInsert.call(this, '  ');
        }
    });

    var editor = $('#LAY_editor')
        ,iframeElem = $('#LAY_demo')
        ,runCodes = function(){
        if(!iframeElem[0]) return;
        var html = editor.val();

        var iframeDocument = iframeElem.prop('contentWindow').document;
        iframeDocument.open();
        iframeDocument.write(html);
        iframeDocument.close();

    };
    $('#LAY_demo_run').on('click', runCodes), runCodes();

    //让导航在最佳位置
    var setScrollTop = function(thisItem, elemScroll){
        if(thisItem[0]){
            var itemTop = thisItem.offset().top
                ,winHeight = $(window).height();
            if(itemTop > winHeight - 160){
                elemScroll.animate({'scrollTop': itemTop - winHeight/2}, 200);
            }
        }
    }

    //让选中的菜单保持在可视范围内
    util.toVisibleArea({
        scrollElem: $('.layui-side-scroll').eq(0)
        ,thisElem: $('.site-demo-nav').find('dd.layui-this')
    });

    util.toVisibleArea({
        scrollElem: $('.layui-side-scroll').eq(1)
        ,thisElem: $('.site-demo-table-nav').find('li.layui-this')
    });



    //查看代码
    $(function(){
        var DemoCode = $('#LAY_democode');
        DemoCode.val([
            DemoCode.val()
            ,'<body>'
            ,global.preview
            ,'\n<script src="//res.layui.com/layui/dist/layui.js" charset="utf-8"></script>'
            ,'\n<!-- 注意：如果你直接复制所有代码到本地，上述 JS 路径需要改成你本地的 -->'
            ,$('#LAY_democodejs').html()
            ,'\n</body>\n</html>'
        ].join(''));
    });

    //点击查看代码选项
    element.on('tab(demoTitle)', function(obj){
        if(obj.index === 1){
            if(device.ie && device.ie < 9){
                layer.alert('强烈不推荐你通过ie8/9 查看代码！因为，所有的标签都会被格式成大写，且没有换行符，影响阅读');
            }
        }
    })


    //手机设备的简单适配
    var treeMobile = $('.site-tree-mobile')
        ,shadeMobile = $('.site-mobile-shade')

    treeMobile.on('click', function(){
        $('body').addClass('site-mobile');
    });

    shadeMobile.on('click', function(){
        $('body').removeClass('site-mobile');
    });



    //愚人节
    ;!function(){
        if(elemHome.data('date') === '4-1'){
            return
            if(local['20180401']) return;

            elemHome.addClass('site-out-up');
            setTimeout(function(){
                layer.photos({
                    photos: {
                        "data": [{
                            "src": "//cdn.layui.com/upload/2018_4/168_1522515820513_397.png",
                        }]
                    }
                    ,anim: 2
                    ,shade: 1
                    ,move: false
                    ,end: function(){
                        layer.msg('愚公，快醒醒！', {
                            shade: 1
                        }, function(){
                            layui.data('layui', {
                                key: '20180401'
                                ,value: true
                            });
                        });
                    }
                    ,success: function(layero, index){
                        elemHome.removeClass('site-out-up');

                        layero.find('#layui-layer-photos').on('click', function(){
                            layer.close(layero.attr('times'));
                        }).find('.layui-layer-imgsee').remove();
                    }
                });
            }, 1000*3);
        }
    }();

    //获取文档图标数据
    home.getIconData = function(){
        var $ = layui.$
            ,iconData = []
            ,iconListElem = $('.site-doc-icon li');

        iconListElem.each(function(){
            var othis = $(this);
            iconData.push({
                title: $.trim(othis.find('.doc-icon-name').text())
                ,fontclass: $.trim(othis.find('.doc-icon-fontclass').text())
                ,unicode: $.trim(othis.find('.doc-icon-code').html())
            });
        });

        $('.site-h1').html('<textarea style="width: 100%; height: 600px;">'+ JSON.stringify(iconData) + '</textarea>');
    };


    exports('global', home);
});